/**
 * FICHIER DE CONFIGURATION DE JAPA POUR LES TESTS UNITAIRES
 * @description fichier de définition des configs pour les tests unitaires avec JAPA
 * @author Torador
 */

import 'reflect-metadata'
import execa from 'execa'
import { join } from 'path'
import getPort from 'get-port'
import { configure } from 'japa'
import sourceMapSupport from 'source-map-support'

process.env.NODE_ENV = 'testing'
process.env.ADONIS_ACE_CWD = join(__dirname)
sourceMapSupport.install({ handleUncaughtExceptions: false })

async function runMigrations() {
  let retries = 5
  while (retries) {
    try {
      await execa.node('ace', ['migration:run'], { stdio: 'inherit' })
      break
    } catch (error) {
      console.log(error)
      retries -= 1
      console.log(`retries left: ${retries}`)
      // wait 5 seconds
      await new Promise((res) => setTimeout(res, 5000))
    }
  }
}

async function rollbackMigrations() {
  await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
}

async function startHttpServer() {
  const { Ignitor } = await import('@adonisjs/core/build/src/Ignitor')
  process.env.PORT = String(await getPort())
  await new Ignitor(__dirname).httpServer().start()
}

function getTestFiles() {
  let userDefined = process.argv.slice(2)[0]
  if (!userDefined) {
    return ['test/**/*.spec.ts', 'test/**/*.test.ts']
  }

  return `${userDefined.replace(/\.ts$|\.js$/, '')}.ts`
}

/**
 * Configure test runner
 */
configure({
  files: getTestFiles(),
  before: [runMigrations, startHttpServer],
  after: [rollbackMigrations],
})
