import execa from 'execa'
import { BaseCommand } from '@adonisjs/core/build/standalone'

export default class TestRun extends BaseCommand {
  /**
   * Command name is used to run the command
   */
  public static commandName = 'test:run'

  /**
   * Command description is displayed in the "help" output
   */
  public static description =
    'Run all TDD define in test folder. This part is very important for CI/CD pipeline.'

  public static settings = {
    /**
     * Set the following value to true, if you want to load the application
     * before running the command
     */
    loadApp: false,

    /**
     * Set the following value to true, if you want this command to keep running until
     * you manually decide to exit the process
     */
    stayAlive: false,
  }

  public async run() {
    this.logger.info(`${this.colors.blue('RUNNING ALL TESTS IN: ')} ${this.colors.green('test/*')}`)
    await execa.node('-r', ['@adonisjs/assembler/build/register', 'japaFile.ts'], {
      stdio: 'inherit',
    })
  }

  public async completed() {
    this.logger.info('FINISHED.')
  }
}
