[![Codacy Badge](https://app.codacy.com/project/badge/Grade/ef118772f7324bc3bfae1f29a5611f5a)](https://www.codacy.com?utm_source=gitlab.com&utm_medium=referral&utm_content=Torador/surfer-api&utm_campaign=Badge_Grade)

![](/assets/logo.png)

# Surfer-API 🏄‍♂️

Professional Social Network build by **INTECH STUDENT**.

## TEAM

- Jordan Kagmeni (Torador): Technical Lead
- Youcef Laoudi: Developer
- Eddy Koko: Developer

## COORDONATEURS

- BENEDETTI Leonard
- SESSA Romain
- KERBRAT Thomas
- CALDERAN Julien

### STACK 📝

- [Node JS](https://nodejs.org)
- [Adonis JS](https://adonisjs.com)
- [PostgreSQL](https://www.postgresql.org/download/)
- [Japa](https://github.com/thetutlage/japa)
- [Redis](https://redis.io)
- [Docker](https://www.docker.com)

### SGBDs COMPATIBLES ET DRIVERS À INSTALLER 🖇

- **MySQL**: yarn add mysql ✅
- **Microsoft SQL Server**: yarn add mssql ✅
- **Oracle**: yarn add oracledb ✅
- **SQLite**: yarn add sqlite3 ✅

### SOCIALS MEDIAS AUTH 🪄

- **Google** ✅
- **Facebook** ✅
- **Twitter** ✅
- **Linkedin** ✅
- **Discord** ✅
- **GitHub** ✅

## Getting started 🚦

```bash
#To run development server
  yarn dev
  #or
  npm run dev

#To run tests
  yarn test
  #or
  npm run test

#To build application for production
  yarn build
  #or
  npm run build

#To run production server
  yarn start
  #or
  npm run start
```

### Intallation 🦅

1.  Node JS

Après avoir cloné le projet, rassurer vous d'avoir nodejs installé sur votre machine en tappant cette commande:

```bash
  node --version
  #v16.10.0
```

Si la version de node ne s'affiche pas, je vous invite à la télécharger [ici](https://nodejs.org/en/).

2.  Dépendances

Pour installer les dépendances du projet, tappez cette commande dans votre terminal à la racine du projet:

```bash
  yarn
  #or
  npm install
```

3.  Redis (Requis pour l'authentification API)

Rassurez-vous d'avoir lancer redis sur votre machine. Vous pouvez le faire via Docker. Voir [ici](https://hub.docker.com/_/redis/)

### Configuration 👨‍💻

1.  Création de la base de données dans votre SGBD

Connectez-vous à votre SGBD de choix et créer trois BD:

- une pour le dev ;
- une pour les test ;
- une pour la prod.

  **Note:** vous pouvez toujours décider de créer une seule BD et de travailler avec, celà ne cause aucun problème au fonctionnement de l'application à condition de renseigner le nom dans les variables d'environnements des différents mode.

2.  Variables d'environnements

- Veuillez créer votre fichier .env à la racine du projet.

- Copiez le contenu se trouvant dans le fichier .env.example et coller le dans votre fichier .env

- Modifiez les identifiants de connexion à votre SGBD sans supprimer les autres. Vous pouvez aussi modifier les variables d'environnements du serveur d'application (PORT, HOST, APP_NAME, etc...).

  **Node**: Par défaut, l'application intègre le driver de PostgresSQL. Si vous utiliseriz un autre SGBD, veuillez installer son driver correspondant pour le bon fonctionnement du système 😎. Conférez le fichier _config/database.ts_ pour plus de détails.

### Migrations & CLI

1. Migrations

   Bravô, vous pouvez maintenant lancer vos migrations. Pour ce faire, tapez la commande:

   ```bash
     node ace migration:run
   ```

   Et pour rollback, tapez:

   ```bash
     node ace migration:rollback
   ```

**Node**: rassurez-vous que votre SGBD soit lancé et écoute sur le bon port.

2. CLI

   a. Lancer les tests Unitaires

   ```bash
   node ace test:run
   ```

   b. Lancer le serveur de developpement

   ```bash
   node ace serve
   #or
   node ace serve --watch #mode watcher
   ```

   c. Construire l'application de production

   ```bash
   node ace build --production
   ```

   d. Lancer le serveur de production

   ```bash
   node server.js
   ```

   e. Lancer le REPL

   ```bash
     node ace repl
   ```

   f. Voir la liste des routes

   ```bash
   node ace list:routes
   ```

   g. Autres

   Pour voir toutes les commandes, tapez:

   ```bash
     node ace
   ```

## Test and Deploy 🚀

Use the built-in continuous integration in GitLab.

- [Get started with GitLab CI/CD](https://gitlab.com/-/experiment/new_project_readme_content:b1b6d6cb9fb897636b48b40aef6476ae?https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://gitlab.com/-/experiment/new_project_readme_content:b1b6d6cb9fb897636b48b40aef6476ae?https://docs.gitlab.com/ee/user/application_security/sast/)
- [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://gitlab.com/-/experiment/new_project_readme_content:b1b6d6cb9fb897636b48b40aef6476ae?https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [Use pull-based deployments for improved Kubernetes management](https://gitlab.com/-/experiment/new_project_readme_content:b1b6d6cb9fb897636b48b40aef6476ae?https://docs.gitlab.com/ee/user/clusters/agent/)

## License 🤓

MIT.
