import { Server } from 'socket.io'
import AdonisServer from '@ioc:Adonis/Core/Server'
import Application from '@ioc:Adonis/Core/Application'

class Ws {
  public io: Server
  private booted = false

  public boot() {
    /**
     * Ignore multiple calls to the boot method
     */
    if (this.booted) {
      return
    }

    this.booted = true
    this.io = new Server(AdonisServer.instance!, {
      cors: {
        origin: [Application.env.get('FRONT_HOST'), 'http://localhost:3000'],
      },
    })
  }
}

export default new Ws()
