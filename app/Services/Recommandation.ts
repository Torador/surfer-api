import Account from 'App/Models/Account'
import Post from 'App/Models/Post'

class RecommandationSystem {
  private userId: number

  constructor(userId: number) {
    this.userId = userId
  }

  private async getUserAccount(): Promise<Account[]> {
    return await Account.query()
      .select('*')
      .where('userId', this.userId)
      .preload('followers', (builder) => builder.preload('contacts'))
      .preload('contacts', (builder) => builder.preload('followers'))
  }

  private async getAccountsPostReacted(account: Account): Promise<Account[]> {
    return await Account.query()
      .select('*')
      .whereNot('id', account.id) //[{...ac}]
      .preload('posts', (builder) => builder.preload('accounts'))
      .preload('contacts', (builder) => builder.preload('followers'))
  }

  private async getSimilarityBetweenAccountsAndUserOnPostReacted(): Promise<any[]> {
    const [accountUser] = await this.getUserAccount()
    const ID_ACCOUNT_FOLLOWERS = accountUser.contacts[0].followers.map((f) => f.id)
    /**
     * TODO: 1- Recuperer la liste des contacts excepté lui-même
     * 2- liste des comptes utilisateurs contenant les objets de leur post publiéé avec les comptes ayant reagi sur ces posts
     * @example // [{...acc, posts: [...p, accounts: [...a]]}]
     */
    const accountsPostReacted = await this.getAccountsPostReacted(accountUser)

    return accountsPostReacted.map((acc) => {
      return {
        ...acc.$attributes,
        isFollowBackShow: ID_ACCOUNT_FOLLOWERS.includes(acc.id),
        contacts: acc.contacts,
        posts: acc.posts.map((post) => {
          return {
            ...post,
            accounts: post.accounts.filter((a) => a.id === accountUser.id),
          }
        }),
      }
    })
  }

  public async getAccountWithScore() {
    const [accountUser] = await this.getUserAccount()
    return (await this.getSimilarityBetweenAccountsAndUserOnPostReacted())
      .map((acc: Account) => {
        const communPost = acc.posts.filter((post: Post) => post.accounts.length === 1)
        if (communPost.length === 0) return { ...acc, score: 0 }

        const distance = Math.sqrt(
          communPost.reduce(
            (a: number, p: Post) =>
              a + (p.$extras.pivot_rating - p.accounts[0].$extras.pivot_rating) ** 2,
            0
          )
        )

        return { ...acc, score: 1 / (1 + distance) }
      })
      .filter((account) => {
        if (!accountUser.followers[0].contacts.length) return account
        for (const following of accountUser.followers[0].contacts) {
          if (following.accountId !== account.id) return account
        }
      })
      .sort((a, b) => b.score - a.score)
      .filter((acc) => acc.score)
  }
}

export default RecommandationSystem
