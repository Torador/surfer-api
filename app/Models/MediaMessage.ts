import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Message from './Message'

export default class MediaMessage extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'message_id' })
  public messageId: number

  @column({ columnName: 'media_name' })
  public mediaName: string

  @column({ columnName: 'url_media' })
  public urlMedia: string

  @column({ columnName: 'mime_type' })
  public mimeType: string

  @column()
  public size: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Message, {
    localKey: 'messageId',
  })
  public messages: BelongsTo<typeof Message>
}
