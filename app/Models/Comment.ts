import { DateTime } from 'luxon'
import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  computed,
  HasMany,
  hasMany,
  ManyToMany,
  manyToMany,
} from '@ioc:Adonis/Lucid/Orm'
import Post from './Post'
import Account from './Account'
import ReplyComment from './ReplyComment'

export default class Comment extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'post_id' })
  public postId: number

  @column({ columnName: 'account_id' })
  public accountId: number

  @column()
  public content: string

  @column({ columnName: 'is_delete' })
  public isDelete: boolean = false

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Post, {
    localKey: 'postId',
  })
  public posts: BelongsTo<typeof Post>

  @belongsTo(() => Account, {
    localKey: 'accountId',
  })
  public account: BelongsTo<typeof Account>

  @hasMany(() => ReplyComment, {
    foreignKey: 'commentId',
  })
  public replies: HasMany<typeof ReplyComment>

  @manyToMany(() => Account, {
    pivotTable: 'rating_comments',
    pivotTimestamps: true,
    pivotRelatedForeignKey: 'account_id',
    pivotColumns: ['rating'],
  })
  public accounts: ManyToMany<typeof Account>

  @computed()
  public get Rating() {
    return this.$extras.pivot_rating
  }
}
