import Encryption from '@ioc:Adonis/Core/Encryption'
import { DateTime } from 'luxon'
import {
  BaseModel,
  beforeCreate,
  BelongsTo,
  belongsTo,
  column,
  computed,
  HasMany,
  hasMany,
  ManyToMany,
  manyToMany,
} from '@ioc:Adonis/Lucid/Orm'
import User from './User'
import Conversation from './Conversation'
import Notification from './Notification'
import Setting from './Setting'
import Post from './Post'
import Comment from './Comment'
import ReplyComment from './ReplyComment'
import Message from './Message'
import Contact from './Contact'
import Follower from './Follower'

export default class Account extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'user_id' })
  public userId: number

  @belongsTo(() => User, {
    localKey: 'userId',
  })
  public user: BelongsTo<typeof User>

  @column()
  public firstname: string

  @column()
  public lastname: string

  @column.date()
  public birthdate: DateTime

  @column()
  public slug: string

  @column()
  public address: string

  @column({ columnName: 'code_postal' })
  public codePostal: string

  @column()
  public city: string

  @column()
  public phone: string

  @column()
  public email: string

  @column()
  public photo: string

  @column()
  public job: string | undefined

  @column()
  public bio: string

  @column()
  public visibility: 'private' | 'public'

  @column()
  public code_backup: string

  @column()
  public status: boolean = false

  @column({ columnName: 'is_lock' })
  public isLock: boolean = false

  @column({ columnName: 'is_delete' })
  public isDeleted: boolean = false

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => Conversation, {
    foreignKey: 'accountInit',
  })
  public conversationsInit: HasMany<typeof Conversation>

  @hasMany(() => Setting, {
    foreignKey: 'accountId',
  })
  public settings: HasMany<typeof Setting>

  @hasMany(() => Notification, {
    foreignKey: 'accountId',
  })
  public notifications: HasMany<typeof Notification>

  @hasMany(() => Contact, {
    foreignKey: 'accountId',
  })
  public contacts: HasMany<typeof Contact>

  @hasMany(() => Comment, {
    foreignKey: 'accountId',
  })
  public comment: HasMany<typeof Comment>

  @hasMany(() => ReplyComment, {
    foreignKey: 'accountId',
  })
  public replies: HasMany<typeof ReplyComment>

  @hasMany(() => Follower, {
    foreignKey: 'accountId',
  })
  public followers: HasMany<typeof Follower>

  @hasMany(() => Conversation, {
    foreignKey: 'accountInter',
  })
  public conversationsInter: HasMany<typeof Conversation>

  @hasMany(() => Message, {
    foreignKey: 'fromAccountId',
  })
  public fromMessages: HasMany<typeof Message>

  @hasMany(() => Message, {
    foreignKey: 'toAccountId',
  })
  public toMessages: HasMany<typeof Message>

  @hasMany(() => Post, {
    foreignKey: 'accountId',
  })
  public creators: HasMany<typeof Post>

  @manyToMany(() => Post, {
    pivotTable: 'rating_posts',
    pivotTimestamps: true,
    pivotColumns: ['rating'],
  })
  public posts: ManyToMany<typeof Post>

  @manyToMany(() => Comment, {
    pivotTable: 'rating_comments',
    pivotTimestamps: true,
    pivotColumns: ['rating'],
  })
  public comments: ManyToMany<typeof Comment>

  @beforeCreate()
  public static assignUuid(account: Account) {
    account.slug = Encryption.encrypt(Date.now().toString(16))
  }

  @computed()
  public getFullName() {
    return `${this.firstname} ${this.lastname}`
  }

  @computed()
  public get shortName() {
    return this.firstname && this.lastname
      ? `${this.firstname?.split(' ')[0]} ${this.lastname?.split(' ')[0]}`
      : null
  }

  @computed()
  public get initialName() {
    return this.shortName
      ? `${this.shortName.split(' ')[0][0]}${this.shortName.split(' ')[1][0]}`
      : null
  }

  @computed()
  public get Rating(): number {
    return this.$extras.pivot_rating
  }
}
