import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Account from './Account'

export default class Setting extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'account_id' })
  public accountId: number

  @column({ columnName: 'mute_notifications' })
  public muteNotifications: boolean = false

  @column({ columnName: 'hide_status' })
  public hideStatus: boolean = false

  @column({ columnName: 'mail_alert' })
  public mailAlert: boolean = false

  @column({ columnName: 'phone_alert' })
  public phoneAlert: boolean = false

  @column({ columnName: 'cookies_permission' })
  public cookiesPermission: boolean = false

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Account, {
    localKey: 'accountId',
  })
  public accounts: BelongsTo<typeof Account>
}
