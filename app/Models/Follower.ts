import { DateTime } from 'luxon'
import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  ManyToMany,
  manyToMany,
} from '@ioc:Adonis/Lucid/Orm'
import Contact from './Contact'
import Account from './Account'

export default class Follower extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'account_id' })
  public accountId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Account, {
    localKey: 'accountId',
  })
  public accounts: BelongsTo<typeof Account>

  @manyToMany(() => Contact, {
    pivotTable: 'is_follows',
    pivotTimestamps: true,
  })
  public contacts: ManyToMany<typeof Contact>
}
