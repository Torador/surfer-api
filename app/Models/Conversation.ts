import Encryption from '@ioc:Adonis/Core/Encryption'
import { DateTime } from 'luxon'
import {
  BaseModel,
  beforeCreate,
  BelongsTo,
  belongsTo,
  column,
  HasMany,
  hasMany,
} from '@ioc:Adonis/Lucid/Orm'
import Account from './Account'
import Message from './Message'

export default class Conversation extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column()
  public room: string

  @column({ columnName: 'account_id' })
  public accountInit: number

  @column({ columnName: 'interlocutor' })
  public accountInter: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Account, {
    localKey: 'accountInit',
  })
  public initiator: BelongsTo<typeof Account>

  @belongsTo(() => Account, {
    localKey: 'accountInter',
  })
  public interlocutor: BelongsTo<typeof Account>

  @hasMany(() => Message, {
    foreignKey: 'conversationId',
  })
  public messages: HasMany<typeof Message>

  @beforeCreate()
  public static assignUuid(conversation: Conversation) {
    conversation.room = Encryption.encrypt(Date.now().toString(16))
  }
}
