import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Post from './Post'

export default class FilePost extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'post_id' })
  public postId: number

  @column()
  public urlImg: string

  @column()
  public filename: string

  @column({ columnName: 'mime_type' })
  public mimeType: string

  @column()
  public size: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Post, {
    localKey: 'postId',
  })
  public posts: BelongsTo<typeof Post>
}
