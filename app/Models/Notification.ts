import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column } from '@ioc:Adonis/Lucid/Orm'
import Account from './Account'

export default class Notification extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'account_id' })
  public accountId: number

  @column()
  public title: string

  @column()
  public description: string

  @column({ columnName: 'link_redirect' })
  public linkRedirect: string

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Account, {
    localKey: 'accountId',
  })
  public accounts: BelongsTo<typeof Account>
}
