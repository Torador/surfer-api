import { DateTime } from 'luxon'
import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  manyToMany,
  ManyToMany,
} from '@ioc:Adonis/Lucid/Orm'
import Account from './Account'
import Follower from './Follower'

export default class Contact extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'account_id' })
  public accountId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Account, {
    localKey: 'accountId',
  })
  public accounts: BelongsTo<typeof Account>

  @manyToMany(() => Follower, {
    pivotTable: 'is_follows',
    pivotTimestamps: true,
  })
  public followers: ManyToMany<typeof Follower>
}
