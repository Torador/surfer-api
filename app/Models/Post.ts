import Encryption from '@ioc:Adonis/Core/Encryption'
import { DateTime } from 'luxon'
import {
  BaseModel,
  beforeCreate,
  BelongsTo,
  belongsTo,
  column,
  computed,
  HasMany,
  hasMany,
  ManyToMany,
  manyToMany,
} from '@ioc:Adonis/Lucid/Orm'
import Account from './Account'
import Comment from './Comment'
import Topic from './Topic'
import FilePost from './FilePost'

export default class Post extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'account_id' })
  public accountId: number

  @column()
  public slug: string

  @column()
  public title: string

  @column()
  public content: string

  @column({ columnName: 'is_comment_lock' })
  public isCommentLock: boolean = false

  @column({ columnName: 'is_delete' })
  public isDelete: boolean = false

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @hasMany(() => Comment, {
    foreignKey: 'postId',
  })
  public comments: HasMany<typeof Comment>

  @hasMany(() => FilePost, {
    foreignKey: 'postId',
  })
  public files: HasMany<typeof FilePost>

  @belongsTo(() => Account, {
    localKey: 'accountId',
  })
  public account: BelongsTo<typeof Account>

  @manyToMany(() => Account, {
    pivotTable: 'rating_posts',
    pivotTimestamps: true,
    pivotRelatedForeignKey: 'account_id',
    pivotColumns: ['rating'],
  })
  public accounts: ManyToMany<typeof Account>

  @manyToMany(() => Topic, {
    pivotTable: 'topic_posts',
    pivotTimestamps: true,
  })
  public topics: ManyToMany<typeof Topic>

  @beforeCreate()
  public static assignUuid(post: Post) {
    post.slug = Encryption.encrypt(Date.now().toString(16))
  }

  @computed()
  public get Rating(): number {
    return this.$extras.pivot_rating
  }
}
