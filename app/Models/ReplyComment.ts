import { DateTime } from 'luxon'
import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  computed,
  ManyToMany,
  manyToMany,
} from '@ioc:Adonis/Lucid/Orm'
import Comment from './Comment'
import Account from './Account'
export default class ReplyComment extends BaseModel {
  @column({ isPrimary: true })
  public id: number
  @column({ columnName: 'account_id' })
  public accountId: number
  @column({ columnName: 'comment_id' })
  public commentId: number
  @column()
  public content: string
  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime
  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime
  @belongsTo(() => Comment, {
    localKey: 'commentId',
  })
  public comments: BelongsTo<typeof Comment>
  @belongsTo(() => Account, {
    localKey: 'accountId',
  })
  public accounts: BelongsTo<typeof Account>
  @manyToMany(() => Account, {
    pivotTable: 'rating_replies',
    pivotTimestamps: true,
    pivotForeignKey: 'reply_id',
    pivotRelatedForeignKey: 'account_id',
    pivotColumns: ['rating'],
  })
  public replies: ManyToMany<typeof Account>
  @computed()
  public get Rating() {
    return this.$extras.pivot_rating
  }
}
