import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, HasMany, hasMany } from '@ioc:Adonis/Lucid/Orm'
import Conversation from './Conversation'
import Account from './Account'
import MediaMessage from './MediaMessage'

export default class Message extends BaseModel {
  @column({ isPrimary: true })
  public id: number

  @column({ columnName: 'conversation_id' })
  public conversationId: number

  @column({ columnName: 'from_account_id' })
  public fromAccountId: number

  @column({ columnName: 'to_account_id' })
  public toAccountId: number

  @column()
  public content: string

  @column()
  public format: string

  @column({ columnName: 'is_delete' })
  public isDelete: boolean = false

  @column({ columnName: 'have_seen' })
  public haveSeen: boolean = false

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(() => Conversation, {
    localKey: 'conversationId',
  })
  public conversations: BelongsTo<typeof Conversation>

  @belongsTo(() => Account, {
    localKey: 'fromAccountId',
  })
  public from: BelongsTo<typeof Account>

  @belongsTo(() => Account, {
    localKey: 'toAccountId',
  })
  public to: BelongsTo<typeof Account>

  @hasMany(() => MediaMessage, {
    foreignKey: 'messageId',
  })
  public mediaMessage: HasMany<typeof MediaMessage>
}
