import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Topic from 'App/Models/Topic'
import TopicValidator from 'App/Validators/TopicValidator'

import Utilities from '../../../utils'

export default class TopicsController {
  public async create({ request, response }: HttpContextContract) {
    const topic = await request.validate(TopicValidator)

    try {
      const newTopic = await Topic.firstOrCreate(topic)

      return response.created(newTopic)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async index({ response }: HttpContextContract) {
    const topics = await Topic.all()

    return response.ok(topics)
  }
}
