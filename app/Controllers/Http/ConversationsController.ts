import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Account from 'App/Models/Account'
import Conversation from 'App/Models/Conversation'
import ConversationValidator from 'App/Validators/ConversationValidator'
import { PAGE_NUMBER_INVALID, ACCOUNT_NOT_FOUND } from './../../../utils/const'
import Utilities from '../../../utils/index'

export default class ConversationsController {
  public async create({ auth, request, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const payload = await request.validate(ConversationValidator)

    try {
      const account = await Account.findBy('user_id', usr?.id)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

      if (account.id === payload.accountInter)
        return response.badRequest({
          message: 'Votre message doit être envoyé à une autre personne que vous.',
        })

      const conversation = await Conversation.query()
        .select('*')
        .where('accountInit', payload.accountInter)
        .andWhere('accountInter', account.id)

      if (conversation.length) return response.ok(conversation)

      const newConversation = await Conversation.firstOrCreate({
        ...payload,
        accountInit: account.id,
      })

      await newConversation.load('messages')

      return response.created(newConversation)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  /*******************pagination *****************************/
  public async index({ auth, params, response }: HttpContextContract) {
    const limit: number = parseInt(params.limit) ?? 10
    const page: number = parseInt(params.page)

    if (isNaN(page)) return response.badRequest({ message: PAGE_NUMBER_INVALID })
    const usr = auth.use('api').user
    const account = await Account.findBy('user_id', usr?.id)
    if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

    try {
      const conversations = await Conversation.query()
        .select('*')
        .where('accountInit', account.id)
        .orWhere('accountInter', account.id)
        .orderBy('createdAt', 'desc')
        .preload('messages', (query) => query.orderBy('createdAt', 'desc').groupLimit(1))

        .paginate(page, limit ? limit : 10)

      return response.ok(conversations.serialize())
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async showAll({ auth, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const account = await Account.findBy('user_id', usr?.id)
    if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

    try {
      const conversations = await Conversation.query()
        .select('*')
        .where('accountInit', account.id)
        .orWhere('accountInter', account.id)
        .orderBy('createdAt', 'desc')
        .preload('messages', (query) => query.orderBy('createdAt', 'desc').groupLimit(1))

      return response.ok(conversations)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async delete({ params, response }: HttpContextContract) {
    const id = parseInt(params.id)

    const conversation = await Conversation.findBy('id', id)
    if (!conversation) return response.notFound({ message: "Cette conversation n'existe pas." })

    await conversation.delete()
    return response.ok({ message: 'Deleted' })
  }
}
