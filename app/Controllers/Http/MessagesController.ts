import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import Hash from '@ioc:Adonis/Core/Hash'
import Message from 'App/Models/Message'
import MessageValidator from 'App/Validators/MessageValidator'
import Account from 'App/Models/Account'
import { PAGE_NUMBER_INVALID } from './../../../utils/const'
import Utilities from '../../../utils/index'
import Conversation from 'App/Models/Conversation'
import Ws from 'App/Services/Ws'

export default class MessagesController {
  public async create({ auth, request, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const message = await request.validate(MessageValidator)

    try {
      const account = await Account.findByOrFail('userId', usr?.id)

      const conversation = await Conversation.findBy('id', message.conversationId)
      if (!conversation) return response.notFound({ message: "Cette conversation n'existe pas." })

      const data = {
        fromAccountId: 0,
        toAccountId: 0,
      }
      if (conversation.accountInit === account.id) {
        data.fromAccountId = account.id
        data.toAccountId = conversation.accountInter
      }
      if (conversation.accountInter === account.id) {
        data.fromAccountId = account.id
        data.toAccountId = conversation.accountInit
      }

      if (!data.fromAccountId || !data.toAccountId)
        return response.unauthorized({
          message: 'Vous ne pouvez pas envoyer de message dans cette conversation',
        })

      const newMessage = await Message.create({ ...message, ...data })

      if (message.media && message.media.length > 0) {
        message.media.forEach(async (file) => {
          if (file === undefined) return
          const f = `Surfer${(await Hash.make(newMessage.id.toString())).replace(
            '$argon2id$v=19$t=3,m=4096,p=1$',
            ''
          )}${Date.now()}.${file.extname}`
          file.clientName = f
          try {
            await file.move(Application.publicPath('media'))

            await newMessage.related('mediaMessage').create({
              mediaName: f,
              mimeType: `image/${file.extname}`,
              size: file.size,
              urlMedia: `${request.protocol()}://${Application.env.get(
                'HOST'
              )}:${Application.env.get('PORT')}/media/${f}`,
            })
          } catch (error) {
            return Utilities.ControllerException(response, error)
          }
        })
      }
      await newMessage.load('mediaMessage')
      const c = await Conversation.findBy('id', message.conversationId)
      if (!c) return response.notFound({ msg: "La conversation n'existe pas" })
      Ws.io.emit('message:comming', newMessage)

      return response.created(newMessage)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  /*******************pagination *****************************/
  public async index({ params, response }: HttpContextContract) {
    const limit: number = parseInt(params.limit) ?? 10
    const page: number = parseInt(params.page)
    const conversationId = parseInt(params.conversationId)

    if (isNaN(page) || page < 0 || isNaN(conversationId))
      return response.badRequest({ message: PAGE_NUMBER_INVALID })

    try {
      const messages = await Message.query()
        .select('*')
        .where('isDelete', false)
        .where('conversationId', conversationId)
        .preload('mediaMessage')
        .paginate(page, limit)

      return response.ok(messages.serialize())
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async showAll({ params, response }) {
    const conversationId = parseInt(params.conversationId)
    if (isNaN(conversationId)) return response.badRequest({ message: PAGE_NUMBER_INVALID })

    try {
      const messages = await Message.query()
        .select('*')
        .where('isDelete', false)
        .where('conversationId', conversationId)
        .preload('mediaMessage')

      return response.ok(messages)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async delete({ params, response }: HttpContextContract) {
    const id = parseInt(params.id)

    const message = await Message.findBy('id', id)
    if (!message) return response.notFound({ message: "Ce message n'existe pas" })

    await message.delete()
    return response.ok({ message: 'Deleted' })
  }
}
