import Application from '@ioc:Adonis/Core/Application'
import { ACCOUNT_LOCKED, UNAUTHORIZED_PRIVILEGES } from './../../../utils/const'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Account from 'App/Models/Account'
import AccountValidator from 'App/Validators/AccountValidator'
import DriveConfig from '@ioc:Adonis/Core/Drive'
import {
  ACCOUNT_DELETED,
  ACCOUNT_NOT_FOUND,
  PAGE_NUMBER_INVALID,
  USER_NOT_FOUND,
} from '../../../utils/const'
import Utilities from '../../../utils/index'

//const Helpers = use('Helpers')

/**
 * @class AccountsController
 * @description Controlleur contenant les méthodes métiers du model Account
 * @author Torador, Youcef
 */
export default class AccountsController {
  public async create({ request, response }: HttpContextContract) {
    const account = await request.validate(AccountValidator)

    if (!account.user_id) return response.unauthorized({ message: UNAUTHORIZED_PRIVILEGES })

    try {
      const [isExistByEmail] = await Account.query()
        .select('*')
        .where('email', account.email)
        .where('is_delete', false)
        .where('is_lock', false)
      if (isExistByEmail)
        return response.ok({
          message: `${isExistByEmail.email} Un compte existe déjà. Veuillez réessayer un autre SVP.`,
        })

      const newAccount = await Account.create(account)
      return response.created(newAccount)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  /*******************pagination *****************************/
  public async index({ params, response }: HttpContextContract) {
    const limit: number = parseInt(params.limit) ?? 10
    const page: number = parseInt(params.page)

    if (isNaN(page)) return response.badRequest({ message: PAGE_NUMBER_INVALID })

    try {
      const accounts = await Account.query()
        .select('*')
        .where('is_delete', false)
        .where('is_lock', false)
        .paginate(page, limit)
      return response.ok(accounts.serialize())
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async showAll({ response }: HttpContextContract) {
    try {
      const accounts = await Account.query()
        .select('*')
        .where('is_delete', false)
        .where('is_lock', false)
        .preload('contacts', (builder) =>
          builder.preload('followers', (q) => q.preload('contacts'))
        )
        .preload('followers', (builder) =>
          builder.preload('contacts', (q) => q.preload('followers'))
        )

      return response.ok(accounts)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  //methode qui retourne un compte par son id
  public async show({ auth, params, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const id = isNaN(Number(params.id)) ? usr?.id : parseInt(params.id)

    if (!id) return response.notFound({ message: USER_NOT_FOUND })

    try {
      const account = await Account.findBy(usr?.id ? 'user_id' : 'id', id)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

      if (account.isDeleted) return response.notFound({ message: ACCOUNT_DELETED })

      if (account.isLock) return response.notFound({ message: ACCOUNT_LOCKED })
      await account.load('contacts', (builder) => builder.preload('followers'))
      await account.load('followers', (builder) => builder.preload('contacts'))

      return response.ok(account)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async showBySlug({ params, response }: HttpContextContract) {
    const slug = params.slug
    if (!slug) return response.badRequest({ message: "Votre requetes n'est pas complete. " })

    try {
      const [account] = await Account.query()
        .where('is_delete', false)
        .where('slug', slug)
        .preload('contacts', (builder) => builder.preload('followers'))
        .preload('followers', (builder) => builder.preload('contacts'))
        .withAggregate('creators', (query) => query.count('*').as('creators_total'))
        .preload('creators', (builder) =>
          builder
            .where('isDelete', false)
            .preload('accounts', (query) =>
              query
                .preload('contacts', (q) => q.preload('followers'))
                .preload('followers', (q) => q.preload('contacts'))
            )
            .preload('files')
            .preload('topics')
            .preload('comments', (query) =>
              query
                .where('isDelete', false)
                .orderBy('createdAt', 'desc')
                .preload('replies', (p) => p.preload('replies'))
                .preload('accounts')
            )
            .orderBy('createdAt', 'desc')
        )
        .preload('posts', (builder) =>
          builder
            .where('isDelete', false)
            .preload('accounts', (query) => query.preload('contacts').preload('followers'))
            .preload('files')
            .preload('topics')
            .preload('comments', (query) =>
              query
                .where('isDelete', false)
                .orderBy('createdAt', 'desc')
                .preload('replies', (p) => p.preload('replies'))
                .preload('accounts')
            )
            .orderBy('createdAt', 'desc')
        )
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

      return response.ok(account)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async update({ auth, request, params, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const id = isNaN(Number(params.id)) ? usr?.id : parseInt(params.id)
    if (!id) return response.notFound({ message: USER_NOT_FOUND })

    const newAccount = await request.validate(AccountValidator)

    const whereKey = isNaN(Number(params.id)) ? 'user_id' : 'id'

    if (whereKey === 'id' && usr?.role === 'user')
      return response.unauthorized(UNAUTHORIZED_PRIVILEGES)

    try {
      const [account] = await Account.query()
        .select('*')
        .where(whereKey, id)
        .where('is_delete', false)
        .where('is_lock', false)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

      const [isExist] = await Account.query()
        .select('*')
        .whereNot(whereKey, id)
        .where('email', newAccount.email)
        .where('is_delete', false)
        .where('is_lock', false)

      if (isExist?.id === id) return response.ok({ message: `${newAccount.email} existe déjà.` })
      const photo = newAccount.picture

      if (photo) {
        const name = `Surfer_${account.slug}.${photo.extname}`
        const hostname = `${Application.env.get('HOST')}:${Application.env.get('PORT')}/profiles/`
        if (account.photo) {
          const isOnLocalDrive = account.photo.includes(hostname)
          if (isOnLocalDrive) {
            const [, previousUrlFile] = account.photo.split(hostname)
            await DriveConfig.delete(previousUrlFile)
          }
        }

        await photo.move(Application.publicPath('profiles'), {
          name,
          overwrite: true,
        })
        account.photo = `${request.protocol()}://${Application.env.get(
          'HOST'
        )}:${Application.env.get('PORT')}/profiles/${name}`
      }

      account.firstname = newAccount.firstname
      account.lastname = newAccount.lastname
      account.birthdate = newAccount.birthdate
      account.address = newAccount.address || account.address
      account.codePostal = newAccount.codePostal || account.codePostal
      account.city = newAccount.city || account.city
      account.phone = newAccount.phone || account.phone
      account.bio = newAccount.bio || account.bio
      account.job = newAccount.job

      await account.save()

      await account.load('contacts', (builder) => builder.preload('followers'))
      await account.load('followers', (builder) => builder.preload('contacts'))

      return response.ok(account)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  // public async download({ params, response }: HttpContextContract) {
  //   const filePath = `profiles/${params.fileName}`
  //   const isExist = await DriveConfig.exists(filePath)

  //   if (isExist) {
  //     return response.download(Application.tmpPath(filePath))
  //   }
  //   return 'File does not exist'
  // }

  public async updateVisibility({ auth, params, response }: HttpContextContract) {
    const visibility: 'public' | 'private' = params.visibility
    if (visibility !== 'private' && visibility !== 'public')
      return response.badRequest({
        message: 'Veuillez renseigner une visibilitée pour votre compte. "public" ou "private" ?',
      })

    const usr = auth.use('api').user
    const id = isNaN(Number(params.id)) ? usr?.id : parseInt(params.id)
    if (!id) return response.notFound({ message: USER_NOT_FOUND })

    const whereKey = usr?.id ? 'user_id' : 'id'
    if (whereKey === 'id' && usr?.role === 'user')
      return response.unauthorized(UNAUTHORIZED_PRIVILEGES)

    try {
      const account = await Account.findBy(whereKey, id)
      if (!account) return response.status(404).json({ error: true, msg: ACCOUNT_NOT_FOUND })

      account.visibility = visibility || 'public'
      await account.save()
      return response.ok(account)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async toggleLock({ auth, params, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const id = isNaN(Number(params.id)) ? usr?.id : parseInt(params.id)
    if (!id) return response.notFound({ message: USER_NOT_FOUND })

    const whereKey = usr?.id ? 'user_id' : 'id'
    if (whereKey === 'id' && usr?.role === 'user')
      return response.unauthorized(UNAUTHORIZED_PRIVILEGES)

    try {
      const account = await Account.findBy(whereKey, id)
      if (!account) return response.notFound({ error: true, msg: ACCOUNT_NOT_FOUND })

      account.isLock = !account.isLock
      await account.save()
      return response.ok({
        message: `${
          account.isLock ? 'Compte bloqué avec succès.' : 'Compte débloqué avec succès.'
        }`,
      })
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async destroyOrUndo({ auth, params, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const paramId = Number(params.id)
    const id = isNaN(paramId) ? usr?.id : parseInt(params.id)
    if (!id) return response.notFound({ message: USER_NOT_FOUND })

    const whereKey = isNaN(paramId) ? 'user_id' : 'id'

    try {
      if (whereKey === 'id' && usr?.role === 'user') {
        const c = await Account.findBy(whereKey, id)
        if (!c) return response.notFound({ message: ACCOUNT_NOT_FOUND })
        if (c.userId !== usr?.id) return response.unauthorized({ message: UNAUTHORIZED_PRIVILEGES })

        c.isDeleted = !c.isDeleted
        await c.save()
        return response.ok({
          message: `${
            c.isDeleted ? 'Compte supprimé avec succès.' : 'Compte restauré avec succès.'
          }`,
        })
      }

      const account = await Account.findBy(whereKey, id)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

      account.isDeleted = !account.isDeleted
      await account.save()
      return response.ok({
        message: `${
          account.isDeleted ? 'Compte supprimé avec succès.' : 'Compte restauré avec succès.'
        }`,
      })
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }
}
