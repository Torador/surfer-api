import { ID_NOT_DEFINE, PAGE_NUMBER_INVALID, POST_NOT_FOUND } from './../../../utils/const'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Account from 'App/Models/Account'
import Comment from 'App/Models/Comment'
import CommentValidator from 'App/Validators/CommentValidator'
import Utilities from '../../../utils'
import { ACCOUNT_NOT_FOUND, COMMENT_NOT_FOUND } from '../../../utils/const'
import RateCommentValidator from 'App/Validators/RateCommentValidator'
import Post from 'App/Models/Post'

export default class CommentsController {
  public async create({ auth, request, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const commentaire = await request.validate(CommentValidator)

    try {
      const account = await Account.findBy('id', usr?.id)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

      const post = await Post.find(commentaire.postId)
      if (!post) return response.notFound({ message: POST_NOT_FOUND })

      if (post.isCommentLock)
        return response.unauthorized({
          message: "Les commentaires de ce post ont été désactivés par l'auteur",
        })

      const newCommentaire = await Comment.create({
        content: commentaire.content,
        postId: commentaire.postId,
        accountId: account.id,
      })
      //TODO: File Comment Upload

      return response.created(newCommentaire)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async index({ params, response }: HttpContextContract) {
    const limit: number = parseInt(params.limit) ?? 10
    const page: number = parseInt(params.page)

    if (isNaN(page) || page < 0) return response.badRequest({ message: PAGE_NUMBER_INVALID })

    try {
      const comment = await Comment.query()
        .select('*')
        .where('isDelete', false)
        .paginate(page, limit)
      return response.ok(comment.serialize())
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async getByPost({ params, response }: HttpContextContract) {
    const postId = parseInt(params.postId)
    if (postId === 0 || isNaN(postId)) {
      return response.badRequest({ message: 'mauvais identifiant' })
    }
    const comments = await Comment.query()
      .select('*')
      .where('post_id', postId)
      .where('isDelete', false)
      .orderBy('createdAt', 'desc')
      .preload('accounts')
      .preload('replies', (builder) => builder.preload('replies'))

    if (!comments) return response.notFound({ message: "Ce post n'a pas encore de commentaires." })

    return response.ok(comments || [])
  }

  //update
  public async update({ request, params, response }: HttpContextContract) {
    const id = Number(params.id)
    if (isNaN(id)) return response.badRequest({ message: "Votre requete n'est pas correcte." })

    const newComment = await request.validate(CommentValidator)

    try {
      const [comment] = await Comment.query().select('*').where('isDelete', false).where('id', id)
      if (!comment) return response.notFound({ message: COMMENT_NOT_FOUND })

      comment.content = newComment.content

      await comment.save()

      return response.ok(comment)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  //noter un commentaire
  public async rate({ auth, request, response }: HttpContextContract) {
    const usr = auth.use('api').user

    const rate = await request.validate(RateCommentValidator)
    try {
      const account = await Account.findBy('userId', usr?.id)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })
      const comment = await Comment.find(rate.comment_id)
      if (!comment) return response.notFound({ message: COMMENT_NOT_FOUND })
      const alreadyCommentsRate = await Comment.query().preload('accounts', (query) => {
        query.pivotColumns(['rating'])
      })

      if (alreadyCommentsRate.length <= 0)
        return response.notFound({ message: 'Aucun Commentaire.' })

      const [currentCommentToRate] = alreadyCommentsRate.filter((cmt) => cmt.id === rate.comment_id)
      // Verifie si le compte a déjà voté
      const [currentAccRated] = currentCommentToRate.accounts.filter((acc) => acc.id === account.id)

      if (!currentAccRated) {
        await comment.related('accounts').attach({
          [account.id]: {
            rating: rate.rating,
          },
        })
        await comment.load('accounts')
        await comment.load('replies')
        return response.created(comment)
      }

      await comment.related('accounts').sync(
        {
          [account.id]: {
            rating: rate.rating,
          },
        },
        false
      )
      await currentCommentToRate.load('accounts')
      await currentCommentToRate.load('replies')

      return response.created(currentCommentToRate)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  /* Supprimer un commentaire */
  public async delete({ params, response }: HttpContextContract) {
    const id: number = parseInt(params.id)

    if (isNaN(id)) return response.badRequest({ message: ID_NOT_DEFINE })

    const comment = await Comment.find(params.id)
    if (!comment) {
      return response.notFound({ message: COMMENT_NOT_FOUND })
    }

    comment.isDelete = !comment.isDelete

    await comment.save()

    return response.ok({
      message: `${
        comment.isDelete ? 'Commentaire supprimé avec succès.' : 'Commentaire restauré avec succès.'
      }`,
    })
  }
}
