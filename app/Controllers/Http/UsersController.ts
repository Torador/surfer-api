import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Account from 'App/Models/Account'
import Contact from 'App/Models/Contact'
import Follower from 'App/Models/Follower'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import {
  ID_NOT_DEFINE,
  INVALID_CREDENTIALS,
  PAGE_NUMBER_INVALID,
  UNAUTHORIZED_PRIVILEGES,
  USER_NOT_FOUND,
} from '../../../utils/const'
import Utilities, { responseAfterAuth } from '../../../utils/index'
import Ws from 'App/Services/Ws'

/**
 * @class UsersController
 * @description Controlleur contenant les méthodes métiers du model User
 * @author Torador, Youcef
 */
export default class UsersController {
  public async login({ auth, request, response }: HttpContextContract) {
    let usr: User
    const apiAuth = auth.use('api')
    if (apiAuth.isLoggedIn) {
      await apiAuth.authenticate()
      return response.ok(apiAuth.token)
    }
    const user = await request.validate(UserValidator)

    try {
      const isExist = await Account.findBy('email', user.email)
      if (!isExist) return response.notFound({ message: USER_NOT_FOUND })
      usr = await User.findByOrFail('id', isExist.userId)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }

    try {
      const token = await apiAuth.attempt(usr.username, user.password, {
        name: `surfer_token_${user.username}`,
        expiresIn: '365days',
      })
      return response
        .cookie('surfer_token', token.toJSON().token, { expires: token.expiresAt?.toJSDate() })
        .ok(token.toJSON())
    } catch {
      return response.badRequest({ message: INVALID_CREDENTIALS })
    }
  }

  public async logout({ auth, response }: HttpContextContract) {
    const apiAuth = auth.use('api')
    const username = apiAuth.user?.username
    Ws.io.emit('load:user', { msg: `${username} are leave.` })
    await apiAuth.revoke()
    return response
      .cookie('surfer_token', null, { expires: new Date(0) })
      .ok({ message: `À la prochaine ${username}.` })
  }

  public async showAll({ response }: HttpContextContract) {
    try {
      const users = (await User.query().select('*').preload('accounts')).filter(
        (usr) => usr.role === 'user'
      )
      return response.ok(users)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }
  /*******************pagination *****************************/
  public async index({ auth, params, response }: HttpContextContract) {
    if (auth.use('api').user?.role === 'user')
      return response.unauthorized({ message: UNAUTHORIZED_PRIVILEGES })

    const limit: number = parseInt(params.limit) ?? 10
    const page: number = parseInt(params.page)

    if (isNaN(page) || page < 0) return response.badRequest({ message: PAGE_NUMBER_INVALID })

    try {
      const users = await User.query().select('*').where('role', 'user').paginate(page, limit)

      return response.ok(users.serialize())
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async show({ auth, params, response }: HttpContextContract) {
    const usr = auth.use('api').user
    //

    const id = isNaN(Number(params.id)) ? usr?.id : parseInt(params.id)

    if (!id) return response.notFound({ message: USER_NOT_FOUND })
    if (usr?.id !== id && usr?.role === 'user')
      return response.unauthorized({ message: UNAUTHORIZED_PRIVILEGES })

    try {
      const user = await User.find(id)
      if (!user) return response.notFound({ message: USER_NOT_FOUND })

      await user.load('accounts', (builder) => {
        builder
          .preload('contacts', (b) => b.preload('followers'))
          .preload('followers', (b) => b.preload('contacts'))
      })

      return response.ok(user)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async register({ request, response }: HttpContextContract) {
    const user = await request.validate(UserValidator)

    try {
      const isExist = await User.findBy('username', user.username)
      if (isExist) return response.badRequest({ message: `${user.username} existe déjà.` })
      const alreadyExist = await Account.findBy('email', user.email)
      if (alreadyExist) return response.badRequest({ message: 'Cet email existe déjà !' })

      const newUser = await User.create({
        username: user.username,
        password: user.password,
        role: user.role,
      })

      console.log(newUser)

      await newUser.related('accounts').create({ userId: newUser.id, email: user.email })
      const account = await Account.findByOrFail('userId', newUser.id)

      await Contact.create({ accountId: account.id })
      await Follower.create({ accountId: account.id })

      return response.created(newUser)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async update({ auth, request, params, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const id = isNaN(Number(params.id)) ? usr?.id : parseInt(params.id)
    if (!id) return response.notFound({ message: USER_NOT_FOUND })

    const newUser = await request.validate(UserValidator)

    try {
      const isExist = await User.findBy('username', newUser.username)
      if (isExist && isExist.id !== id && usr?.role === 'user')
        return response.unauthorized({ message: UNAUTHORIZED_PRIVILEGES })

      if (isExist && isExist.id !== id)
        return response.unauthorized({
          message: `${newUser.username} ne peut pas être attribué à quelqu'un d'autre car il existe déjà.`,
        })

      const user = await User.find(id)
      if (!user) return response.status(404).json({ message: USER_NOT_FOUND })

      user.username = newUser.username
      user.password = newUser.password
      user.role = usr?.role === 'user' ? 'user' : (newUser.role as 'admin' | 'user') || user.role

      await user.save()

      await user.load('accounts')

      return response.ok(user)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async delete({ auth, params, response }: HttpContextContract) {
    if (auth.use('api').user?.role === 'user')
      return response.unauthorized({
        message: UNAUTHORIZED_PRIVILEGES,
      })
    const id: number = parseInt(params.id) || auth.use('api').user?.id || Number('')
    if (isNaN(id)) return response.badRequest({ message: ID_NOT_DEFINE })
    if (id === 0) return response.notFound({ message: USER_NOT_FOUND })

    try {
      const user = await User.find(id)
      if (!user) return response.notFound({ error: true, message: USER_NOT_FOUND })

      await user.delete()

      return response.ok({ message: 'utilisateur supprimé' })
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async updateRole({ auth, params, response }: HttpContextContract) {
    const usr = auth.use('api').user
    if (usr?.role === 'user')
      return response.unauthorized({
        message: UNAUTHORIZED_PRIVILEGES,
      })

    const role: 'user' | 'admin' = params.role
    if (role !== 'admin' && role !== 'user')
      return response.badRequest({
        message: 'Veuillez renseigner un role pour votre utilisateur. "user" ou "admin" ?',
      })
    const id = isNaN(Number(params.id)) ? usr?.id : parseInt(params.id)
    if (!id) return response.notFound({ message: USER_NOT_FOUND })

    try {
      const user = await User.find(id)
      if (!user) return response.notFound({ error: true, message: USER_NOT_FOUND })

      user.role = role || user.role
      await user.save()
      await user.load('accounts')

      return response.ok(user)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  // ALL METHOD FOR AUTH DELAGATE

  public async authWithGithub({ ally, auth, response }: HttpContextContract) {
    const github = ally.use('github')

    /**
     * User has explicitly denied the login request
     */
    if (github.accessDenied()) {
      return response.unauthorized('Access Denied')
    }

    /**
     * Unable to verify the CSRF state
     */
    if (github.stateMisMatch()) {
      return 'Request expired. Retry again'
    }

    /**
     * There was an unknown error during the redirect
     */
    if (github.hasError()) {
      return github.getError()
    }

    /**
     * Finally, access the user
     */
    const githubUser = await github.user()

    try {
      /**
       * Find the user by email or create
       * a new one
       */
      const user = await User.firstOrCreate(
        {
          username: githubUser.name || githubUser.nickName,
        },
        {
          username: githubUser.name,
          password: githubUser.email ?? Date.now().toString(),
        }
      )
      await Account.firstOrCreate(
        { userId: user.id },
        {
          userId: user.id,
          photo: githubUser.avatarUrl ?? '',
          firstname: githubUser.name,
          lastname: githubUser.nickName,
          email: githubUser.email ?? '',
        }
      )

      const token = await auth.use('api').login(user)
      return responseAfterAuth(response, token)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async authWithGoogle({ ally, auth, response }: HttpContextContract) {
    const google = ally.use('google')

    /**
     * User has explicitly denied the login request
     */
    if (google.accessDenied()) {
      return response.unauthorized('Access Denied')
    }

    /**
     * Unable to verify the CSRF state
     */
    if (google.stateMisMatch()) {
      return 'Request expired. Retry again'
    }

    /**
     * There was an unknown error during the redirect
     */
    if (google.hasError()) {
      return google.getError()
    }

    /**
     * Finally, access the user
     */
    const googleUser = await google.user()

    try {
      /**
       * Find the user by email or create
       * a new one
       */
      const user = await User.firstOrCreate(
        {
          username: googleUser.name || googleUser.nickName,
        },
        {
          username: googleUser.name,
          password: '',
        }
      )
      const [lastname, firstname] = googleUser.nickName.split(' ')
      await Account.firstOrCreate(
        googleUser.email ? { userId: user.id, email: googleUser.email } : { userId: user.id },
        {
          userId: user.id,
          photo: googleUser.avatarUrl ?? '',
          firstname,
          lastname,
          email: googleUser.email ?? `${googleUser.name}@surfer.com`,
        }
      )
      const account = await Account.findByOrFail('userId', user.id)

      await Contact.firstOrCreate({ accountId: account.id }, { accountId: account.id })
      await Follower.firstOrCreate({ accountId: account.id }, { accountId: account.id })

      const token = await auth.use('api').login(user)

      return responseAfterAuth(response, token)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async authWithTwitter({ ally, auth, response }: HttpContextContract) {
    const twitter = ally.use('twitter')

    /**
     * User has explicitly denied the login request
     */
    if (twitter.accessDenied()) {
      return response.unauthorized('Access Denied')
    }

    /**
     * Unable to verify the CSRF state
     */
    if (twitter.stateMisMatch()) {
      return 'Request expired. Retry again'
    }

    /**
     * There was an unknown error during the redirect
     */
    if (twitter.hasError()) {
      return twitter.getError()
    }

    /**
     * Finally, access the user
     */
    const twitterUser = await twitter.user()

    try {
      /**
       * Find the user by email or create
       * a new one
       */
      const user = await User.firstOrCreate(
        {
          username: twitterUser.name || twitterUser.nickName,
        },
        {
          username: twitterUser.name,
          password: twitterUser.email ?? Date.now().toString(),
        }
      )
      await Account.firstOrCreate(
        { userId: user.id },
        {
          userId: user.id,
          photo: twitterUser.avatarUrl ?? '',
          firstname: twitterUser.name,
          lastname: twitterUser.nickName,
          email: twitterUser.email ?? '',
        }
      )

      const token = await auth.use('api').login(user)
      return responseAfterAuth(response, token)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async authWithDiscord({ ally, auth, response }: HttpContextContract) {
    const discord = ally.use('discord')

    /**
     * User has explicitly denied the login request
     */
    if (discord.accessDenied()) {
      return response.unauthorized('Access Denied')
    }

    /**
     * Unable to verify the CSRF state
     */
    if (discord.stateMisMatch()) {
      return 'Request expired. Retry again'
    }

    /**
     * There was an unknown error during the redirect
     */
    if (discord.hasError()) {
      return discord.getError()
    }

    /**
     * Finally, access the user
     */
    const discordUser = await discord.user()

    try {
      /**
       * Find the user by email or create
       * a new one
       */
      const user = await User.firstOrCreate(
        {
          username: discordUser.name || discordUser.nickName,
        },
        {
          username: discordUser.name,
          password: discordUser.email ?? Date.now().toString(),
        }
      )
      await Account.firstOrCreate(
        { userId: user.id },
        {
          userId: user.id,
          photo: discordUser.avatarUrl ?? '',
          firstname: discordUser.name,
          lastname: discordUser.nickName,
          email: discordUser.email ?? '',
        }
      )

      const token = await auth.use('api').login(user)
      return responseAfterAuth(response, token)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async authWithLinkedin({ ally, auth, response }: HttpContextContract) {
    const linkedin = ally.use('linkedin')

    /**
     * User has explicitly denied the login request
     */
    if (linkedin.accessDenied()) {
      return response.unauthorized('Access Denied')
    }

    /**
     * Unable to verify the CSRF state
     */
    if (linkedin.stateMisMatch()) {
      return 'Request expired. Retry again'
    }

    /**
     * There was an unknown error during the redirect
     */
    if (linkedin.hasError()) {
      return linkedin.getError()
    }

    /**
     * Finally, access the user
     */
    const linkedinUser = await linkedin.user()

    try {
      /**
       * Find the user by email or create
       * a new one
       */
      const user = await User.firstOrCreate(
        {
          username: linkedinUser.name || linkedinUser.nickName,
        },
        {
          username: linkedinUser.name,
          password: linkedinUser.email ?? Date.now().toString(),
        }
      )
      await Account.firstOrCreate(
        { userId: user.id },
        {
          userId: user.id,
          photo: linkedinUser.avatarUrl ?? '',
          firstname: linkedinUser.name,
          lastname: linkedinUser.nickName,
          email: linkedinUser.email ?? '',
        }
      )

      const token = await auth.use('api').login(user)
      return responseAfterAuth(response, token)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async authWithFacebook({ ally, auth, response }: HttpContextContract) {
    const facebook = ally.use('facebook')

    /**
     * User has explicitly denied the login request
     */
    if (facebook.accessDenied()) {
      return response.unauthorized('Access Denied')
    }

    /**
     * Unable to verify the CSRF state
     */
    if (facebook.stateMisMatch()) {
      return 'Request expired. Retry again'
    }

    /**
     * There was an unknown error during the redirect
     */
    if (facebook.hasError()) {
      return facebook.getError()
    }

    /**
     * Finally, access the user
     */
    const facebookUser = await facebook.user()

    try {
      /**
       * Find the user by email or create
       * a new one
       */
      const user = await User.firstOrCreate(
        {
          username: facebookUser.name || facebookUser.nickName,
        },
        {
          username: facebookUser.name,
          password: facebookUser.email ?? Date.now().toString(),
        }
      )
      await Account.firstOrCreate(
        { userId: user.id },
        {
          userId: user.id,
          photo: facebookUser.avatarUrl ?? '',
          firstname: facebookUser.name,
          lastname: facebookUser.nickName,
          email: facebookUser.email ?? '',
        }
      )

      const token = await auth.use('api').login(user)
      return responseAfterAuth(response, token)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }
}
