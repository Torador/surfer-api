import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Application from '@ioc:Adonis/Core/Application'
import Hash from '@ioc:Adonis/Core/Hash'
import RatePostValidator from 'App/Validators/RatePostValidator'
import Account from 'App/Models/Account'
import Post from 'App/Models/Post'
import PostValidator from 'App/Validators/PostValidator'
import Utilities from '../../../utils'
import {
  ACCOUNT_NOT_FOUND,
  ID_NOT_DEFINE,
  PAGE_NUMBER_INVALID,
  POST_DELETED,
  POST_NOT_FOUND,
  USER_NOT_FOUND,
} from '../../../utils/const'
import Topic from 'App/Models/Topic'

export default class PostsController {
  public async create({ auth, request, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const post = await request.validate(PostValidator)

    const tp = post.topics

    if (
      (tp.length <= 0 && post.topicNew && post.topicNew.length <= 0) ||
      (tp.length <= 0 && post.topicNew === undefined)
    )
      return response.badRequest({ message: "Veuillez définir un centre d'intérêt à votre post." })

    try {
      const topicIds = [...tp]
      const account = await Account.findBy('userId', usr?.id)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })
      if (post.topicNew) {
        post.topicNew.forEach(async (tp) => {
          const t = await Topic.create(tp)
          topicIds.push(t.id)
        })
      }

      const newPost = await Post.create({
        title: post.title,
        content: post.content,
        isCommentLock: post.isCommentLock ?? false,
        accountId: account.id,
      })

      const t = await Topic.findMany(topicIds)

      if (t.length > 0) await newPost.related('topics').attach(t.map((t) => t.id)) // table d'association

      if (post.files && post.files.length > 0) {
        post.files.forEach(async (file) => {
          if (file === undefined) return
          const f = `Surfer${(await Hash.make(newPost.id.toString())).replace(
            '$argon2id$v=19$t=3,m=4096,p=1$',
            ''
          )}${Date.now()}.${file.extname}`
          file.clientName = f
          try {
            await file.move(Application.publicPath('posts'))

            await newPost.related('files').create({
              filename: f,
              mimeType: `image/${file.extname}`,
              size: file.size,
              urlImg: `${request.protocol()}://${Application.env.get('HOST')}:${Application.env.get(
                'PORT'
              )}/posts/${f}`,
            })
          } catch (error) {
            return Utilities.ControllerException(response, error)
          }
        })
      }

      await newPost.load('accounts')
      await newPost.load('comments')
      await newPost.load('topics')
      await newPost.load('files')

      return response.created(newPost)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  //noter un post
  public async rate({ auth, request, response }: HttpContextContract) {
    const usr = auth.use('api').user

    const rate = await request.validate(RatePostValidator)
    try {
      const account = await Account.findBy('userId', usr?.id)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

      const post = await Post.find(rate.post_id)
      if (!post) return response.notFound({ message: POST_NOT_FOUND })
      const alreadyPostsRate = await Post.query().preload('accounts', (query) => {
        query.pivotColumns(['rating'])
      })

      if (alreadyPostsRate.length <= 0) return response.notFound({ message: 'Aucun Post.' })

      const [currentPostToRate] = alreadyPostsRate.filter((apr) => apr.id === rate.post_id)
      // Verifie si le compte a déjà voté
      const [currentAccRated] = currentPostToRate.accounts.filter((acc) => acc.id === account.id)

      if (!currentAccRated) {
        await post.related('accounts').attach({
          [account.id]: {
            rating: rate.rating,
          },
        })
        await post.load('accounts')
        await post.load('files')
        await post.load('comments', (builder) =>
          builder.orderBy('createdAt', 'desc').preload('replies').preload('accounts')
        )
        await post.load('topics')
        return response.created(post)
      }

      await post.related('accounts').sync(
        {
          [account.id]: {
            rating: rate.rating,
          },
        },
        false
      )
      await currentPostToRate.load('accounts')
      await currentPostToRate.load('files')
      await currentPostToRate.load('comments', (builder) =>
        builder.orderBy('createdAt', 'desc').preload('replies').preload('accounts')
      )
      await currentPostToRate.load('topics')

      return response.created(currentPostToRate)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  //autoriser les commentaires ou pas
  public async authorizeComments({ auth, params, response }: HttpContextContract) {
    const user = auth.use('api').user
    const idPost = parseInt(params.id) // recuperer id du post
    if (!idPost && isNaN(idPost)) {
      return response.badRequest({ message: ID_NOT_DEFINE })
    }
    try {
      const account = await Account.findBy('userId', user?.id)
      if (!account) {
        return response.badRequest({ message: ACCOUNT_NOT_FOUND })
      }
      const post = await Post.find(idPost)
      if (!post) {
        return response.notFound({ message: POST_NOT_FOUND })
      }
      post.isCommentLock = !post?.isCommentLock
      await post.save()
      return response.ok({
        message: post.isCommentLock
          ? 'les commentaires sont désactivés'
          : 'les commentaires sont activés',
      })
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  /*******************pagination *****************************/
  public async index({ params, response }: HttpContextContract) {
    const limit: number = parseInt(params.limit) ?? undefined
    const page: number = parseInt(params.page)

    if (isNaN(page) || page < 0) return response.badRequest({ message: PAGE_NUMBER_INVALID })

    try {
      const post = await Post.query()
        .preload('files')
        .preload('topics')
        .preload('comments', (builder) =>
          builder
            .where('isDelete', false)
            .orderBy('createdAt', 'desc')
            .preload('replies')
            .preload('accounts')
        )
        .preload('accounts')
        .select('*')
        .where('isDelete', false)
        .orderBy('createdAt', 'desc')
        .paginate(page, limit)

      return response.ok(post.serialize())
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  //methode qui retourne un post par son id
  public async show({ params, response }: HttpContextContract) {
    const id: number = parseInt(params.id)

    if (isNaN(id)) return response.badRequest({ message: ID_NOT_DEFINE })
    if (id === 0) return response.notFound({ message: USER_NOT_FOUND })

    try {
      const post = await Post.findBy('id', id)
      if (!post) return response.notFound({ message: POST_NOT_FOUND })

      if (post.isDelete) return response.notFound({ message: POST_DELETED })

      await post.load('accounts')
      await post.load('topics')
      await post.load('files')
      await post.load('comments', (builder) =>
        builder
          .where('isDelete', false)
          .orderBy('createdAt', 'desc')
          .preload('replies')
          .preload('accounts')
      )

      return response.ok(post)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async showBySlug({ params, response }: HttpContextContract) {
    const slug = params.slug
    if (!slug) return response.badRequest({ message: "Votre requete n'est pas valide." })

    try {
      const post = await Post.findByOrFail('slug', slug)
      if (post.isDelete) return response.notFound({ message: POST_DELETED })

      await post.load('accounts')
      await post.load('topics')
      await post.load('files')
      await post.load('comments', (builder) =>
        builder
          .where('isDelete', false)
          .orderBy('createdAt', 'desc')
          .preload('replies')
          .preload('accounts')
      )

      return response.ok(post)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  //editer un post
  public async update({ auth, request, params, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const id = isNaN(Number(params.id)) ? usr?.id : parseInt(params.id)
    if (!id) return response.notFound({ message: USER_NOT_FOUND })

    const newPost = await request.validate(PostValidator)

    const whereKey = isNaN(Number(params.id)) ? 'post_id' : 'id'

    try {
      const [post] = await Post.query().select('*').where(whereKey, id).where('isDelete', false)

      if (!post) return response.notFound({ message: POST_NOT_FOUND })

      post.title = newPost.title || ''
      post.content = newPost.content
      post.related('topics').sync(newPost.topics)

      await post.save()

      await post.load('accounts')
      await post.load('topics')
      await post.load('files')
      await post.load('comments', (builder) =>
        builder
          .where('isDelete', false)
          .orderBy('createdAt', 'desc')
          .preload('replies')
          .preload('accounts')
      )

      return response.ok(post)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  /* Supprimer une ligne */
  public async delete({ params, response }: HttpContextContract) {
    const id: number = parseInt(params.id)

    if (isNaN(id)) return response.badRequest({ message: ID_NOT_DEFINE })

    const post = await Post.find(params.id)
    if (!post) {
      return response.notFound({ message: POST_NOT_FOUND })
    }

    post.isDelete = !post.isDelete

    await post.save()

    return response.ok({
      message: `${
        post.isDelete ? 'Publication supprimée avec succès.' : 'Publication restaurée avec succès.'
      }`,
    })
  }
}
