import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Account from 'App/Models/Account'
import Contact from 'App/Models/Contact'
import Follower from 'App/Models/Follower'
import RecommandationSystem from 'App/Services/Recommandation'
import { ACCOUNT_NOT_FOUND, CONTACT_NOT_FOUND, FOLLOWING_NOT_FOUND } from '../../../utils/const'

export default class FollowsController {
  public async store({ params, response, auth }: HttpContextContract) {
    const usr = auth.use('api').user

    const account = await Account.findBy('user_id', usr?.id)
    if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

    const followerByAccount = await Follower.findBy('account_id', account.id)
    if (!followerByAccount) return response.notFound({ message: 'Your ID follower not found' })

    const followingId = parseInt(params.id)
    if (isNaN(followingId)) return response.badRequest({ message: 'Mauvaise requete' })

    const contact = await Contact.findBy('id', followingId)
    if (!contact) return response.notFound({ message: CONTACT_NOT_FOUND })

    const alreadyFollowed = await Contact.query().preload('followers', (query) => {
      return query.where('contact_id', contact.id).where('follower_id', followerByAccount.id)
    })

    if (alreadyFollowed[0].followers.length > 0)
      return response.ok({ message: 'Vous suivez déja ce compte' })

    await contact.related('followers').attach([followerByAccount.id])

    await contact.load('followers')
    return response.created(contact)
  }

  public async showFollowing({ response, auth }: HttpContextContract) {
    const usr = auth.use('api').user
    const account = await Account.findBy('user_id', usr?.id)
    if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

    const follower = await Follower.findBy('account_id', account.id)
    if (!follower) return response.notFound({ message: FOLLOWING_NOT_FOUND })

    await follower.load('contacts')
    return response.ok(follower)
  }

  public async showFollowers({ response, auth }: HttpContextContract) {
    const usr = auth.use('api').user
    const account = await Account.findBy('user_id', usr?.id)
    if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

    const contact = await Contact.findBy('account_id', account.id)
    if (!contact) return response.notFound({ message: CONTACT_NOT_FOUND })

    await contact.load('followers')
    return response.ok(contact)
  }

  public async isAlreadyFollow({ params, response, auth }: HttpContextContract) {
    const usr = auth.use('api').user
    const account = await Account.findBy('user_id', usr?.id)
    if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

    const followingId = parseInt(params.followingId)
    if (isNaN(followingId)) return response.badRequest({ message: 'Mauvaise requete' })

    const contact = await Contact.findBy('account_id', followingId)
    if (!contact) return response.notFound({ message: CONTACT_NOT_FOUND })

    await contact.load('followers')

    return response.ok({ ok: !!contact.followers.filter((f) => f.accountId === account.id)[0] })
  }

  public async suggestionContact({ auth, response }: HttpContextContract) {
    const usr = auth.use('api')?.user

    if (!usr?.id) return response.ok([])
    const suggestionContact = await new RecommandationSystem(usr.id).getAccountWithScore()

    return response.ok(suggestionContact)
  }

  public async destroy({ params, auth, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const account = await Account.findBy('user_id', usr?.id)
    if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

    const FollowersByAccount = await Follower.findBy('account_id', account.id)
    if (!FollowersByAccount) return response.notFound('Not Found Followers By Account')

    const followingId = parseInt(params.followingId)
    if (isNaN(followingId)) return response.badRequest({ message: 'Mauvaise requete' })

    const contact = await Contact.findBy('id', followingId)
    if (!contact) return response.notFound({ message: CONTACT_NOT_FOUND })

    await contact.related('followers').detach([FollowersByAccount.id])

    return response.ok({ message: 'Deleted' })
  }
}
