import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Account from 'App/Models/Account'
import Comment from 'App/Models/Comment'
import ReplyComment from 'App/Models/ReplyComment'
import RateReplyCommentValidator from 'App/Validators/RateReplyCommentValidator'
import ReplyCommentValidator from 'App/Validators/ReplyCommentValidator'
import Utilities from '../../../utils'
import {
  ACCOUNT_NOT_FOUND,
  COMMENT_NOT_FOUND,
  PAGE_NUMBER_INVALID,
  REPLY_COMMENT_NOT_FOUND,
} from '../../../utils/const'

export default class ReplyCommentsController {
  public async create({ auth, request, response }: HttpContextContract) {
    const usr = auth.use('api').user
    const replyComment = await request.validate(ReplyCommentValidator)

    try {
      const account = await Account.findBy('userId', usr?.id)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })
      const comment = await Comment.findBy('id', replyComment.commentId)
      if (!comment) return response.notFound({ message: COMMENT_NOT_FOUND })

      const newReplyComment = await ReplyComment.create({ ...replyComment, accountId: account.id })

      return response.created(newReplyComment)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async rate({ auth, request, response }: HttpContextContract) {
    const usr = auth.use('api').user

    const rate = await request.validate(RateReplyCommentValidator)
    try {
      const account = await Account.findBy('userId', usr?.id)
      if (!account) return response.notFound({ message: ACCOUNT_NOT_FOUND })

      const replyComment = await ReplyComment.find(rate.reply_id)
      if (!replyComment) return response.notFound({ message: REPLY_COMMENT_NOT_FOUND })
      //console.log('before -2 condition')
      const alreadyReplyCommentsRate = await ReplyComment.query().preload('replies', (query) => {
        query.pivotColumns(['rating'])
      })
      //console.log('before-1 condition')
      if (alreadyReplyCommentsRate.length <= 0)
        return response.notFound({ message: 'Aucune réponse au commentaire.' })

      const [currentReplyCommentToRate] = alreadyReplyCommentsRate.filter(
        (rcr) => rcr.id === rate.reply_id
      )
      if (!currentReplyCommentToRate) return response.notFound({ message: '' })
      // Verifie si le compte a déjà voté
      const [currentAccRated] = currentReplyCommentToRate.replies.filter(
        (acc) => acc.id === account.id
      )

      if (!currentAccRated) {
        //console.log('before1 condition')
        await replyComment.related('replies').attach({
          [account.id]: {
            rating: rate.rating,
          },
        })
        //console.log('after1 condition')
        await replyComment.load('replies')
        return response.created(replyComment)
      }

      await replyComment.related('replies').sync(
        {
          [account.id]: {
            rating: rate.rating,
          },
        },
        false
      )

      await currentReplyCommentToRate.load('replies')

      return response.created(currentReplyCommentToRate)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  //methode qui retourne un reply par son id
  public async show({ params, response }: HttpContextContract) {
    const id = parseInt(params.id)
    try {
      const replycomment = await ReplyComment.findBy('id', id)

      if (!replycomment) return response.notFound({ message: REPLY_COMMENT_NOT_FOUND })

      return response.ok(replycomment)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }
  //pagination
  public async index({ params, response }: HttpContextContract) {
    const limit: number = parseInt(params.limit) ?? 10
    const page: number = parseInt(params.page)

    if (isNaN(page) || page < 0) return response.badRequest({ message: PAGE_NUMBER_INVALID })

    try {
      const replyComment = await ReplyComment.query().select('*').paginate(page, limit)
      return response.ok(replyComment.serialize())
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }

  public async getByComment({ params, response }: HttpContextContract) {
    const commentId = parseInt(params.commentId)
    if (commentId === 0 || isNaN(commentId))
      return response.badRequest({ message: 'mauvais identifiant' })

    const replies = await ReplyComment.query()
      .select('*')
      .where('comment_id', commentId)
      .orderBy('createdAt', 'desc')
      .preload('replies')

    if (!replies)
      return response.notFound({ message: "Ce post n'a pas encore de reponse à ce commentaire." })

    return response.ok(replies || [])
  }

  //update
  public async update({ request, params, response }: HttpContextContract) {
    const id = parseInt(params.id)
    if (isNaN(id)) return response.badRequest({ message: "Votre requete n'est pas valide." })

    const newReplyComment = await request.validate(ReplyCommentValidator)

    try {
      const replycomment = await ReplyComment.findBy('id', id)

      if (!replycomment) return response.notFound({ message: REPLY_COMMENT_NOT_FOUND })

      replycomment.content = newReplyComment.content

      await replycomment.save()

      await replycomment.load('replies')

      return response.ok(replycomment)
    } catch (error) {
      return Utilities.ControllerException(response, error)
    }
  }
  /* Supprimer une reponse */
  public async delete({ params, response }) {
    const id = parseInt(params.id)
    if (isNaN(id)) return response.badRequest({ message: 'identfiant invalide' })

    const replycomment = await ReplyComment.find(params.id)

    if (!replycomment) {
      return response.notFound({ message: REPLY_COMMENT_NOT_FOUND })
    }

    replycomment.related('replies').detach()

    await replycomment.delete()

    return response.ok({
      message: 'Reponse supprimée.',
    })
  }
}
