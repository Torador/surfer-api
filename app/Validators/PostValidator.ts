import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { IMAGES_EXTENSION } from '../../utils/const'

export default class PostValidator {
  constructor(protected ctx: HttpContextContract) {}

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public schema = schema.create({
    title: schema.string.optional({ trim: true }),
    content: schema.string({ trim: true }),
    isCommentLock: schema.boolean.optional(),
    account_id: schema.number.optional([rules.unsigned()]),
    topicNew: schema.array.optional().members(
      schema.object().members({
        label: schema.string({ trim: true }),
        description: schema.string.optional({ trim: true }),
      })
    ),
    topics: schema.array().members(schema.number([rules.unsigned()])),
    files: schema.array
      .optional()
      .members(schema.file({ size: '10mb', extnames: [...IMAGES_EXTENSION, 'mp4'] })),
  })

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */
  public messages = {
    'content.required': "Votre post n'a pas de contenu !",
  }
}
