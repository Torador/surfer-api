import { schema, rules } from '@ioc:Adonis/Core/Validator'
import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import { IMAGES_EXTENSION } from '../../utils/const'

export default class AccountValidator {
  constructor(protected ctx: HttpContextContract) {}

  /*
   * Define schema to validate the "shape", "type", "formatting" and "integrity" of data.
   *
   * For example:
   * 1. The username must be of data type string. But then also, it should
   *    not contain special characters or numbers.
   *    ```
   *     schema.string({}, [ rules.alpha() ])
   *    ```
   *
   * 2. The email must be of data type string, formatted as a valid
   *    email. But also, not used by any other user.
   *    ```
   *     schema.string({}, [
   *       rules.email(),
   *       rules.unique({ table: 'users', column: 'email' }),
   *     ])
   *    ```
   */
  public schema = schema.create({
    user_id: schema.number.optional([rules.unsigned()]),
    firstname: schema.string({ trim: true }, [rules.minLength(2), rules.maxLength(100)]),
    lastname: schema.string({ trim: true }, [rules.minLength(2), rules.maxLength(100)]),
    birthdate: schema.date({ format: 'dd/mm/yyyy' }),
    address: schema.string.optional({ trim: true }, [rules.maxLength(255)]),
    codePostal: schema.string.optional({ trim: true }, [rules.maxLength(20)]),
    city: schema.string({ trim: true }, [rules.minLength(2), rules.maxLength(100)]),
    phone: schema.string.optional({ trim: true }, [rules.minLength(8), rules.maxLength(15)]),
    email: schema.string({ trim: true }, [rules.email()]),
    picture: schema.file.optional({ size: '2mb', extnames: IMAGES_EXTENSION }),
    bio: schema.string.optional({ trim: true }, [rules.minLength(0)]),
    visibility: schema.enum.optional(['private', 'public'] as const),
    job: schema.string.optional({ trim: true }),
  })

  /**
   * Custom messages for validation failures. You can make use of dot notation `(.)`
   * for targeting nested fields and array expressions `(*)` for targeting all
   * children of an array. For example:
   *
   * {
   *   'profile.username.required': 'Username is required',
   *   'scores.*.number': 'Define scores as valid numbers'
   * }
   *
   */

  public messages = {
    'firstname.required': 'veuillez entrer votre prenom ',
    'lastname.required': 'veuillez entrer votre nom ',
    //'birthdate.required': 'veuillez entrer votre date de naissance ',
    'email.required': 'veuillez entrer votre adresse mail ',
  }
}
