import Application from '@ioc:Adonis/Core/Application'
/**
 * Config source: https://git.io/JesV9
 *
 * Feel free to let us know via PR, if you find something broken in this config
 * file.
 */

import Env from '@ioc:Adonis/Core/Env'
import { DatabaseConfig } from '@ioc:Adonis/Lucid/Database'

const databaseConfig: DatabaseConfig = {
  /*
  |--------------------------------------------------------------------------
  | Connection
  |--------------------------------------------------------------------------
  |
  | The primary connection for making database queries across the application
  | You can use any key from the `connections` object defined in this same
  | file.
  |
  */
  connection: Env.get('DB_CONNECTION'),

  connections: {
    /*
    |--------------------------------------------------------------------------
    | sqlite config
    |--------------------------------------------------------------------------
    |
    | Configuration for sqlite database. Make sure to install the driver
    | from npm when using this connection
    |
    | npm i sqlite3 or yarn add sqlite3
    |
    */
    sqlite: {
      client: 'sqlite3',
      connection: {
        filename: Env.get('SQLITE_PATH') || Application.tmpPath('db.sqlite3'),
        mode: true,
      },
      migrations: {
        naturalSort: true,
        tableName: 'surfer_schema',
      },
      useNullAsDefault: true,
      healthCheck: false,
      debug: false,
    },

    /*
    |--------------------------------------------------------------------------
    | PostgreSQL config
    |--------------------------------------------------------------------------
    |
    | Configuration for PostgreSQL database. Make sure to install the driver
    | from npm when using this connection
    |
    | npm i pg or yarn add pg
    |
    */
    pg: {
      client: 'pg',
      connection: {
        host: Env.get('PG_HOST', 'localhost'),
        port: Env.get('PG_PORT', 5432),
        user: Env.get('PG_USER', 'postgres'),
        password: Env.get('PG_PASSWORD', 'postgres'),
        database:
          Env.get('NODE_ENV') === 'testing'
            ? Env.get('PG_DB_TEST', 'surfertest')
            : Env.get('NODE_ENV') === 'production'
            ? Env.get('PG_DB_NAME', 'surfer')
            : Env.get('PG_DB_DEV', 'surferdev'),
      },
      migrations: {
        naturalSort: true,
        tableName: 'surfer_schema',
        disableRollbacksInProduction: Env.get('NODE_ENV') === 'development',
      },
      healthCheck: true,
      debug: true,
    },

    /*
    |--------------------------------------------------------------------------
    | MYSQL config
    |--------------------------------------------------------------------------
    |
    | Configuration for MYSQL database. Make sure to install the driver
    | from npm when using this connection
    |
    | npm i mysql or yarn add mysql
    |
    */
    mysql: {
      client: 'mysql',
      connection: {
        host: Env.get('MYSQL_HOST'),
        port: Env.get('MYSQL_PORT'),
        user: Env.get('MYSQL_USER'),
        password: Env.get('MYSQL_PASSWORD'),
        database:
          Env.get('NODE_ENV') === 'testing'
            ? Env.get('MYSQL_DB_TEST', 'surfertest')
            : Env.get('NODE_ENV') === 'production'
            ? Env.get('MYSQL_DB_NAME', 'surfer')
            : Env.get('MYSQL_DB_DEV', 'surferdev'),
      },
      migrations: {
        naturalSort: true,
        tableName: 'surfer_schema',
        disableRollbacksInProduction: Env.get('NODE_ENV') === 'development',
      },
      healthCheck: Env.get('NODE_ENV') === 'development',
      debug: true,
    },

    /*
    |--------------------------------------------------------------------------
    | Microsoft Server SQL (MSSQL) config
    |--------------------------------------------------------------------------
    |
    | Configuration for MSSQL database. Make sure to install the driver
    | from npm when using this connection
    |
    | npm i mssql or yarn add mssql
    |
    */
    mssql: {
      client: 'mssql',
      connection: {
        server: Env.get('MSSQL_SERVER'),
        port: Env.get('MSSQL_PORT'),
        user: Env.get('MSSQL_USER'),
        password: Env.get('MSSQL_PASSWORD'),
        database:
          Env.get('NODE_ENV') === 'testing'
            ? Env.get('MSSQL_DB_TEST', 'surfertest')
            : Env.get('NODE_ENV') === 'production'
            ? Env.get('MSSQL_DB_NAME', 'surfer')
            : Env.get('MSSQL_DB_DEV', 'surferdev'),
      },
      migrations: {
        naturalSort: true,
        tableName: 'surfer_schema',
        disableRollbacksInProduction: Env.get('NODE_ENV') === 'development',
      },
      healthCheck: false,
      debug: false,
    },

    /*
    |--------------------------------------------------------------------------
    | Oracle config
    |--------------------------------------------------------------------------
    |
    | Configuration for Oracle database. Make sure to install the driver
    | from npm when using this connection
    |
    | npm i oracledb or yarn add oracledb
    |
    */
    oracle: {
      client: 'oracledb',
      connection: {
        host: Env.get('ORACLE_HOST'),
        port: Env.get('ORACLE_PORT'),
        user: Env.get('ORACLE_USER'),
        password: Env.get('ORACLE_PASSWORD'),
        database:
          Env.get('NODE_ENV') === 'testing'
            ? Env.get('ORACLE_DB_TEST', 'surfertest')
            : Env.get('NODE_ENV') === 'production'
            ? Env.get('ORACLE_DB_NAME', 'surfer')
            : Env.get('ORACLE_DB_DEV', 'surferdev'),
      },
      migrations: {
        naturalSort: true,
        tableName: 'surfer_schema',
        disableRollbacksInProduction: Env.get('NODE_ENV') === 'development',
      },
      healthCheck: false,
      debug: false,
    },
  },
}

export default databaseConfig
