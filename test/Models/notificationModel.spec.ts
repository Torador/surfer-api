import Account from 'App/Models/Account'
import User from 'App/Models/User'
import Notification from 'App/Models/Notification'
import execa from 'execa'
import test from 'japa'

test.group('Test Notification Model', (group) => {
  let notif = {
    title: 'titre de la notif',
    description: ' jordan a publié un nouveau post',
    linkRedirect: 'lien de la redirection',
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    //user =
    await User.create({ username: 'surfer2', password: 'surferpassword2' })

    await Account.create({
      firstname: 'surfer1',
      lastname: 'surfer1',
      email: `surfer1@surfer.com`,
      userId: 1,
    })
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('Should Save Notification in DB', async (assert) => {
    await Notification.create({
      accountId: 1,
      /*fromAccountId: 0,
      toAccountId: 1,*/
      ...notif,
    })

    const n = await Notification.all()

    assert.equal(n[0].accountId, 1)
    assert.equal(n[0].title, notif.title)
    assert.equal(n[0].description, notif.description)
    assert.equal(n[0].linkRedirect, notif.linkRedirect)
  })
  test('Should Get Notification in DB', async (assert) => {
    assert.isArray(await Notification.all(), 'Error occured on get users')
  })
  test('Should Update Notification in DB', async (assert) => {
    const notif2 = await Notification.create({
      accountId: 1,
      ...notif,
    })
    notif2.title = 'nouveau titre'

    await notif2.save()

    assert.equal(notif2.title, 'nouveau titre')
  })
  test('Should Delete Notification in DB', async (assert) => {
    const notif = await Notification.all()

    await notif[0].delete()

    assert.equal((await Notification.all()).length, notif.length - 1)
  })
})
