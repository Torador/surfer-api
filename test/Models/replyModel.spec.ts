import Account from 'App/Models/Account'
import Comment from 'App/Models/Comment'
import Post from 'App/Models/Post'
import ReplyComment from 'App/Models/ReplyComment'
import User from 'App/Models/User'
import execa from 'execa'
import test from 'japa'

test.group('Test ReplyComment Model', (group) => {
  let user: User
  let account: Account[] = []
  let comment: Comment[] = []
  //let post: Post[] = []
  let rp = {
    content: 'Reponse du commentaire : tout le monde mange la  banane',
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    user = await User.create({ username: 'surfer2', password: 'surferpassword2' })

    const a = await Account.create({
      firstname: 'surfer1',
      lastname: 'surfer1',
      email: `surfer1@surfer.com`,
      userId: user.id,
    })
    await Post.create({
      title: 'poste numéro 1 ',
      content: 'ce poste est le poste numéro : 1',
      // accountId
      accountId: 1,
    })

    const c = await Comment.create({
      content: 'ce commentaire est le commentaire numéro : 1',
      accountId: 1,
      postId: 1,
    })
    account.push(a)
    comment.push(c)
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  //test 1
  test('Should Save Reply in DB', async (assert) => {
    const reply = new ReplyComment()
    reply.commentId = 1
    reply.accountId = 1
    reply.content = rp.content
    await reply.save()
    const r = await ReplyComment.all()

    assert.equal(r[0].accountId, 1)
    assert.equal(r[0].commentId, 1)
    assert.equal(r[0].content, rp.content)
  })
  test('Should Get Reply in DB', async (assert) => {
    assert.isArray(await ReplyComment.all(), 'Error occured on get users')
  })
  test('Should Update ReplyComment in DB', async (assert) => {
    const reply = new ReplyComment()
    reply.commentId = 1
    reply.accountId = 1
    reply.content = rp.content
    await reply.save()

    reply.content = 'je viens de modifier ma reponse'

    await reply.save()

    assert.equal(reply.accountId, 1)
    assert.notEqual(reply.content, rp.content)
    assert.equal(reply.commentId, 1)
  })
  //supprimer
  test('Should Delete Reply in DB', async (assert) => {
    const replys = await ReplyComment.all()

    await replys[0].delete()

    assert.equal((await ReplyComment.all()).length, replys.length - 1)
  })
})
