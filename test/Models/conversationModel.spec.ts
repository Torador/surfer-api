import Account from 'App/Models/Account'
import User from 'App/Models/User'
import Conversation from 'App/Models/Conversation'
import execa from 'execa'
import test from 'japa'

test.group('Test Conversation Model', (group) => {
  let user: User
  let cnv = {
    accountInter: 0,
    accountInit: 0,
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    user = await User.create({ username: 'surfer2', password: 'surferpassword2' })
    for (let i = 1; i < 3; i++) {
      await Account.create({
        firstname: 'surfer' + i,
        lastname: 'surfer' + i,
        email: `surfer${i}@surfer.com`,
        userId: user.id,
      })
    }

    cnv = {
      ...cnv,
      accountInit: 1,
      accountInter: 2,
    }
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('Should Save Conversation in DB', async (assert) => {
    await Conversation.create({
      ...cnv,
    })

    const c = await Conversation.all()
    assert.equal(c[0].accountInit, cnv.accountInit)
    assert.equal(c[0].accountInter, cnv.accountInter)
  })
  test('Should Get Conversation in DB', async (assert) => {
    assert.isArray(await Conversation.all(), 'Error occured on get conversations')
  })
  test('Should Update Conversation in DB', async (assert) => {
    const cc = await Conversation.create({
      ...cnv,
    })
    cc.accountInit = 1

    await cc.save()

    assert.equal(cc.accountInit, 1)
  })
  test('Should Delete Conversation in DB', async (assert) => {
    const c = await Conversation.all()

    await c[0].delete()

    assert.equal((await Conversation.all()).length, c.length - 1)
  })
})
