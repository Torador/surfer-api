import MediaMessage from 'App/Models/MediaMessage'
import Message from 'App/Models/Message'
import User from 'App/Models/User'
import Account from 'App/Models/Account'
import Conversation from 'App/Models/Conversation'
import execa from 'execa'
import test from 'japa'

test.group('Test mediaMessage Model', (group) => {
  let mediamsg = {
    messageId: 1,
    mediaName: 'photo de profil',
    urlMedia: 'http/perso/fichier/img.jpeg',
    size: 1000,
    mimeType: 'mimetype',
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    //user =
    await User.create({ username: 'surfer2', password: 'surferpassword2' })

    await Account.create({
      firstname: 'surfer1',
      lastname: 'surfer1',
      email: `surfer1@surfer.com`,
      userId: 1,
    })
    await Account.create({
      firstname: 'surfer2',
      lastname: 'surfer2',
      email: `surfer2@surfer.com`,
      userId: 1,
    })
    await Conversation.create({
      accountInit: 1,
      accountInter: 2,
    })
    await Message.create({
      content: 'ceci est mon premier message ',
      format: 'txt',
      fromAccountId: 1,
      toAccountId: 2,
      conversationId: 1,
    })
  })
  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })
  test('Should Save MediaMessage in DB', async (assert) => {
    await MediaMessage.create({
      ...mediamsg,
    })
    const mediam = await MediaMessage.all()

    assert.equal(mediam[0].messageId, 1)
    assert.equal(mediam[0].size, mediamsg.size)
    assert.equal(mediam[0].mediaName, mediamsg.mediaName)
    assert.equal(mediam[0].urlMedia, mediamsg.urlMedia)
    assert.equal(mediam[0].mimeType, mediamsg.mimeType)
  })
  test('Should Get MediaMessage in DB', async (assert) => {
    assert.isArray(await MediaMessage.all(), 'Error occured on get users')
  })

  test('Should Update MediaMessage in DB', async (assert) => {
    const mm = await MediaMessage.create({
      ...mediamsg,
    })
    mm.urlMedia = 'nouveau url'

    await mm.save()

    assert.equal(mm.urlMedia, 'nouveau url')
  })
  test('Should Delete MediaMessage in DB', async (assert) => {
    const mm = await MediaMessage.all()

    await mm[0].delete()

    assert.equal((await MediaMessage.all()).length, mm.length - 1)
  })
})
