import Account from 'App/Models/Account'
import User from 'App/Models/User'
import Conversation from 'App/Models/Conversation'
import Message from 'App/Models/Message'
import execa from 'execa'
import test from 'japa'
test.group('Test Message Model', (group) => {
  let msg = {
    content: 'ceci est mon premier message ',
    format: 'txt',
    fromAccountId: 1,
    toAccountId: 2,
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    //user =
    await User.create({ username: 'surfer2', password: 'surferpassword2' })

    await Account.create({
      firstname: 'surfer1',
      lastname: 'surfer1',
      email: `surfer1@surfer.com`,
      userId: 1,
    })
    await Account.create({
      firstname: 'surfer2',
      lastname: 'surfer2',
      email: `surfer2@surfer.com`,
      userId: 1,
    })
    await Conversation.create({
      accountInit: 1,
      accountInter: 2,
    })
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })
  // test 1 : créer un message et le sauvegarder dans la bdd
  test('Should Save Message in DB', async (assert) => {
    await Message.create({
      conversationId: 1,
      /*fromAccountId: 0,
      toAccountId: 1,*/
      ...msg,
    })

    const m = await Message.all()

    assert.equal(m[0].fromAccountId, 1)
    assert.equal(m[0].toAccountId, 2)
    assert.equal(m[0].content, msg.content)
  })
  test('Should Get Message in DB', async (assert) => {
    assert.isArray(await Message.all(), 'Error occured on get users')
  })
  test('Should Update Message in DB', async (assert) => {
    const mm = await Message.create({
      conversationId: 1,
      ...msg,
    })
    mm.content = 'nouveau message'

    await mm.save()

    assert.equal(mm.content, 'nouveau message')
  })
  test('Should Delete Message in DB', async (assert) => {
    const mm = await Message.all()

    await mm[0].delete()

    assert.equal((await Message.all()).length, mm.length - 1)
  })
})
