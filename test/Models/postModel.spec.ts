import Account from 'App/Models/Account'
import Post from 'App/Models/Post'
import User from 'App/Models/User'
import execa from 'execa'
import test from 'japa'

test.group('Test Post Model', (group) => {
  let user: User
  let account: Account[] = []
  let ps = {
    title: 'Bonjour le monde',
    content: 'Tout le monde mange la  banane',
    isCommentLock: false,
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    user = await User.create({ username: 'surfer2', password: 'surferpassword2' })
    for (let i = 1; i < 4; i++) {
      await Account.create({
        firstname: 'surfer' + i,
        lastname: 'surfer' + i,
        email: `surfer${i}@surfer.com`,
        userId: user.id,
      })
    }
    account.push(...(await Account.all()))
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('Should Save Post in DB', async (assert) => {
    const post = await Post.create({
      accountId: account[0].id,
      ...ps,
    })

    const p = await Post.all()
    let isEqual = false
    p.forEach((pst) => {
      if (post.title === pst.title && pst.content === post.content) {
        isEqual = true
      }
    })
    assert.isTrue(isEqual)
  })

  test('Should Get Post in DB', async (assert) => {
    assert.isArray(await Post.all(), 'Error occured on get users')
  })

  test('Should Update Post in DB', async (assert) => {
    const post = await Post.create({
      accountId: account[1].id,
      ...ps,
    })

    post.title = 'Hello Wold'
    post.isCommentLock = true

    await post.save()

    assert.notEqual(post.title, ps.title)
    assert.equal(post.content, ps.content)
    assert.notEqual(post.isCommentLock, ps.isCommentLock)
  })

  test('should get topicPost in DB', async (assert) => {
    await Post.create({
      accountId: account[2].id,
      ...ps,
    })
    const p = await Post.find(1)
    if (!p) throw new Error('POST not found')
    await p.related('topics').create({ label: 'IA', description: 'Artificial Intelligence' })
    await p.related('topics').attach([1])
    const [topic] = await p.related('topics').query()

    assert.equal(topic.$extras.pivot_post_id, 1)
    assert.equal(topic.$extras.pivot_topic_id, 1)

    await p.related('topics').detach([1])
  })

  test('Should Delete Post in DB', async (assert) => {
    const posts = await Post.all()

    await posts[0].delete()

    assert.equal((await Post.all()).length, posts.length - 1)
  })
})
