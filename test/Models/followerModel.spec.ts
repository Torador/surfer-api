import Account from 'App/Models/Account'
import User from 'App/Models/User'
import Follower from 'App/Models/Follower'
import execa from 'execa'
import test from 'japa'

test.group('Test Followers Model', (group) => {
  let user: User
  let account: Account[] = []
  let follower = {}

  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    user = await User.create({ username: 'surfer', password: 'surferpassword' })
    const a = await Account.create({
      firstname: 'surfer1',
      lastname: 'surfer1',
      email: `surfer1@surfer.com`,
      userId: user.id,
    })
    account.push(a)
  })
  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('Should Save Follower in DB', async (assert) => {
    await Follower.create({
      accountId: 1,
      ...follower,
    })

    const f = await Follower.all()

    assert.equal(f[0].accountId, 1)
  })

  test('Should Get Follower in DB', async (assert) => {
    assert.isArray(await Follower.all(), 'Error occured on get users')
  })

  test('Should Update Follower in DB', async (assert) => {
    const followerr = await Follower.create({
      accountId: 1,
      ...follower,
    })
    followerr.accountId = 1

    await followerr.save()

    assert.equal(followerr.accountId, 1)
  })

  test('Should Delete Follower in DB', async (assert) => {
    const followers = await Follower.all()

    await followers[0].delete()

    assert.equal((await Follower.all()).length, followers.length - 1)
  })
})
