import User from 'App/Models/User'
import execa from 'execa'
import test from 'japa'

test.group('Test User Model', (group) => {
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })
  })

  test('Should Save User in DB', async (assert) => {
    const user = new User()
    user.username = 'surfer1234'
    user.password = 'toto'
    user.role = 'user'
    await user.save()

    const u = await User.all()
    let isEqual = false
    u.forEach((usr) => {
      if (user.username === usr.username && user.role === usr.role) {
        isEqual = true
      }
    })
    assert.isTrue(isEqual)
  })

  test('Should Get Users in DB', async (assert) => {
    assert.isArray(await User.all(), 'Error occured on get users')
  })

  test('Should Update User in DB', async (assert) => {
    const user = new User()
    user.username = 'Eifel'
    user.password = 'toto'
    user.role = 'user'
    await user.save()

    const u = await User.find(user.id)
    if (!u) throw new Error('404')
    u.username = 'SURFER'
    u.password = 'toto'
    await u.save()
    assert.equal(u.id, user.id)
    assert.notEqual(u.username, user.username)
  })

  test('Should Delete User in DB', async (assert) => {
    const user = new User()
    user.username = 'adonisjs'
    user.password = 'secret'
    await user.save()

    await user.delete()

    const u = await User.find(user.id)

    assert.isNull(u)
  })

  test('Ensure user password gets hashed during save', async (assert) => {
    const user = new User()
    user.username = 'adonisjs'
    user.password = 'secret'
    await user.save()

    assert.notEqual(user.password, 'secret')
  })
})
