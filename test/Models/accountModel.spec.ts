import Account from 'App/Models/Account'
import User from 'App/Models/User'
import execa from 'execa'
import test from 'japa'

test.group('Test Account Model', (group) => {
  let user: User
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    user = await User.create({ username: 'surfer', password: 'surferpassword' })
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('Should Save Account in DB', async (assert) => {
    const account = new Account()
    account.userId = user.id
    account.firstname = 'surfer'
    account.lastname = 'toto'
    account.email = 'toto@gmail.com'
    await account.save()

    const a = await Account.all()
    let isEqual = false
    a.forEach((act) => {
      if (account.firstname === act.firstname && act.lastname === account.lastname) {
        isEqual = true
      }
    })
    assert.isTrue(isEqual)
  })

  test('Should Get Account in DB', async (assert) => {
    assert.isArray(await Account.all(), 'Error occured on get users')
  })

  test('Should Update Account in DB', async (assert) => {
    const account = await Account.create({
      userId: user.id,
      firstname: 'Eifel',
      lastname: 'toto',
      email: 'Eifel@gmail.com',
    })

    const a = await Account.findBy('user_id', account.userId)
    if (!a) throw new Error('404')
    a.firstname = 'SURFER'
    a.lastname = 'toto'
    a.email = 'Eifel12@gmail.com'
    await a.save()
    assert.notEqual(a.firstname, account.firstname)
    assert.notEqual(a.email, account.email)
  })

  test('Should Delete Account in DB', async (assert) => {
    const s = (await Account.all()).length
    const account = new Account()
    account.userId = user.id
    account.firstname = 'adonisjs'
    account.lastname = 'secret'
    account.email = 'adonisjs@gmail.com'
    await account.save()

    await account.delete()
    const acc = await Account.all()

    assert.equal(acc.length, s)
  })
})
