import Account from 'App/Models/Account'
import User from 'App/Models/User'
import Contact from 'App/Models/Contact'
import execa from 'execa'
import test from 'japa'

test.group('Test Contact Model', (group) => {
  let user: User
  let account: Account[] = []
  let cont = {}

  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    user = await User.create({ username: 'surfer', password: 'surferpassword' })
    const a = await Account.create({
      firstname: 'surfer1',
      lastname: 'surfer1',
      email: `surfer1@surfer.com`,
      userId: user.id,
    })
    account.push(a)
  })
  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('Should Save Contact in DB', async (assert) => {
    await Contact.create({
      accountId: 1,
      ...cont,
    })

    const c = await Contact.all()

    assert.equal(c[0].accountId, 1)
  })

  test('Should Get Contact in DB', async (assert) => {
    assert.isArray(await Contact.all(), 'Error occured on get users')
  })

  test('Should Update Contact in DB', async (assert) => {
    const contact = await Contact.create({
      accountId: 1,
      ...cont,
    })
    contact.accountId = 1

    await contact.save()

    assert.equal(contact.accountId, 1)
  })

  test('Should Delete Contact in DB', async (assert) => {
    const contacts = await Contact.all()

    await contacts[0].delete()

    assert.equal((await Contact.all()).length, contacts.length - 1)
  })
})
