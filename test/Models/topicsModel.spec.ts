import Account from 'App/Models/Account'
import Post from 'App/Models/Post'
import Topic from 'App/Models/Topic'
import User from 'App/Models/User'
import execa from 'execa'
import test from 'japa'

test.group('Test Topic Model', (group) => {
  let user: User
  let account: Account[] = []
  //let comment: Comment[] = []
  let post: Post[] = []
  let tp = {
    label: 'topic 1 ',
    description: 'ceci est le premier topic de Surfer',
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    user = await User.create({ username: 'surfer2', password: 'surferpassword2' })

    const a = await Account.create({
      firstname: 'surfer1',
      lastname: 'surfer1',
      email: `surfer1@surfer.com`,
      userId: user.id,
    })
    const p = await Post.create({
      title: 'poste numéro 1 ',
      content: 'ce poste est le poste numéro : 1',
      // accountId
      accountId: 1,
    })

    account.push(a)
    post.push(p)
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })
  //test 1
  test('Should Save Topic in DB', async (assert) => {
    await Topic.create({
      ...tp,
    })

    const t = await Topic.all()

    assert.equal(t[0].label, tp.label)
    assert.equal(t[0].description, tp.description)
    //assert.equal(r[0].content, rp.content)
  })
  test('Should Get Topic in DB', async (assert) => {
    assert.isArray(await Topic.all(), 'Error occured on get users')
  })
  test('Should Update Topic in DB', async (assert) => {
    const topicc = await Topic.create({
      label: 'label 2',
      description: 'description2',
    })

    await topicc.save()

    assert.notEqual(topicc.label, tp.label)
  })
  test('Should Delete Topic in DB', async (assert) => {
    const top = await Topic.all()

    await top[0].delete()

    assert.equal((await Topic.all()).length, top.length - 1)
  })
})
