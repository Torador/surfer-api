import Account from 'App/Models/Account'
import Comment from 'App/Models/Comment'
import Post from 'App/Models/Post'
import User from 'App/Models/User'
import execa from 'execa'
import test from 'japa'

test.group('Test Comment Model', (group) => {
  let user: User
  let account: Account[] = []
  let post: Post[] = []
  let cm = {
    content: 'Tout le monde mange la  banane',
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    user = await User.create({ username: 'surfer2', password: 'surferpassword2' })

    const a = await Account.create({
      firstname: 'surfer1',
      lastname: 'surfer1',
      email: `surfer1@surfer.com`,
      userId: user.id,
    })
    const p = await Post.create({
      title: 'poste numéro 1 ',
      content: 'ce poste est le poste numéro : 1',
      // accountId
      accountId: 1,
    })
    account.push(a)
    post.push(p)
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })
  // test 1 : créer un commentaire et le sauvegarder dans la bdd
  test('Should Save Comment in DB', async (assert) => {
    await Comment.create({
      accountId: 1,
      postId: 1,
      ...cm,
    })

    const c = await Comment.all()

    assert.equal(c[0].accountId, 1)
    assert.equal(c[0].postId, 1)
    assert.equal(c[0].content, cm.content)
  })
  test('Should Get Comment in DB', async (assert) => {
    assert.isArray(await Comment.all(), 'Error occured on get users')
  })

  test('Should Update Comment in DB', async (assert) => {
    const comment = await Comment.create({
      accountId: 1,
      postId: 1,
      ...cm,
    })

    comment.content = 'je viens de modifier mon commentaire'

    await comment.save()

    assert.equal(comment.accountId, 1)
    assert.notEqual(comment.content, cm.content)
    assert.equal(comment.postId, 1)
  })

  test('Should Delete Comment in DB', async (assert) => {
    const comments = await Comment.all()

    await comments[0].delete()

    assert.equal((await Comment.all()).length, comments.length - 1)
  })
})
