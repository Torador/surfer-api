import Account from 'App/Models/Account'
import Post from 'App/Models/Post'
import FilePost from 'App/Models/FilePost'
import User from 'App/Models/User'
import execa from 'execa'
import test from 'japa'

test.group('Test FilePost Model', (group) => {
  let user: User
  let account: Account[] = []
  let post: Post[] = []
  let fp = {
    urlImg: 'url',
    filename: 'nom du fichier',
    mimeType: 'je sais pas',
    size: 1000,
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    user = await User.create({ username: 'surfer2', password: 'surferpassword2' })

    const a = await Account.create({
      firstname: 'surfer1',
      lastname: 'surfer1',
      email: `surfer1@surfer.com`,
      userId: user.id,
    })
    const p = await Post.create({
      title: 'poste numéro 1 ',
      content: 'ce poste est le poste numéro : 1',
      // accountId
      accountId: 1,
    })
    account.push(a)
    post.push(p)
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })
  test('Should Save FilePost in DB', async (assert) => {
    await FilePost.create({
      postId: 1,
      ...fp,
    })

    const fpp = await FilePost.all()

    assert.equal(fpp[0].postId, 1)
    assert.equal(fpp[0].size, fp.size)
    assert.equal(fpp[0].urlImg, fp.urlImg)
    assert.equal(fpp[0].filename, fp.filename)
    assert.equal(fpp[0].mimeType, fp.mimeType)
  })
  test('Should Get FilePost in DB', async (assert) => {
    assert.isArray(await FilePost.all(), 'Error occured on get users')
  })
  test('Should Update FilePost in DB', async (assert) => {
    const fpp = await FilePost.create({
      postId: 1,
      ...fp,
    })
    fpp.urlImg = 'nouveau Url'

    await fpp.save()

    assert.equal(fpp.urlImg, 'nouveau Url')
  })
  test('Should Delete FilePost in DB', async (assert) => {
    const fp = await FilePost.all()

    await fp[0].delete()

    assert.equal((await FilePost.all()).length, fp.length - 1)
  })
})
