import test from 'japa'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'

test('should check health of Surfer', async (assert) => {
  const report = await HealthCheck.getReport()
  assert.isTrue(report.healthy)
})
