import User from 'App/Models/User'
import RecommandationSystem from 'App/Services/Recommandation'
import test from 'japa'

test.skip('Test Recommandation System', async () => {
  const user = await User.findByOrFail('id', 7)
  const rs = new RecommandationSystem(user.id)

  test('Get Account with score similarity', async (_) => {
    const accountWithScore = await rs.getAccountWithScore()
    console.log(accountWithScore)
  })
})
