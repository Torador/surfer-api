import Application from '@ioc:Adonis/Core/Application'
import test from 'japa'
import request from 'supertest'
import Post from 'App/Models/Post'
import execa from 'execa'
import { user } from './accountController.spec'
import { Jeton } from '../../utils/types'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}/api/v1/posts`

test.group('Test Controller Posts', async (group) => {
  let jetonUser3: Jeton
  let jetonUser1: Jeton

  const post = {
    title: 'My first post',
    content: "Hey, What's up guys ? \n Hope you are well.",
  }

  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}/api/v1/users`

    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...user, username: 'Toto', email: 'toto@gmail.com' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    const userToken = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)

    const userToken2 = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...user, username: 'Toto', email: 'toto@gmail.com' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)

    jetonUser3 = JSON.parse(userToken.text)
    jetonUser1 = JSON.parse(userToken2.text)
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('should returns 422 when save post without topic', async () => {
    const newPost = { ...post }

    await request(BASE_URL)
      .post('')
      .send(newPost)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)
    await request(BASE_URL)
      .post('')
      .send({ ...newPost, topics: [] })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400)
  })

  test('should returns 422 when save post without account id', async () => {
    await request(BASE_URL)
      .post('')
      .send(post)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)
  })

  test('should returns 401 when save post without auth user', async () => {
    await request(BASE_URL).post('').send(post).expect(401)
  })

  test('should returns 201 after save post without images pingle', async (assert) => {
    const newPost = {
      ...post,
      topicNew: [{ label: 'ML', description: 'Machine Learning' }],
      topics: [],
    }
    const res = await request(BASE_URL)
      .post('')
      .send(newPost)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    assert.equal(res.body.title, newPost.title)
    assert.equal(res.body.content, newPost.content)
    assert.equal(res.body.topics[0].label, newPost.topicNew[0].label)
    assert.equal(res.body.topics[0].description, newPost.topicNew[0].description)
  })

  test('should returns 201 after save post with images pingle', async (assert) => {
    const newPost = {
      ...post,
      topics: [1],
    }

    const res = await request(BASE_URL)
      .post('')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .field('title', post.title)
      .field('content', newPost.content)
      .field('topics[]', newPost.topics)
      .field('isCommentLock', false)
      .expect(201)
      .attach('files[]', Application.makePath('assets/logo.png'))
      .attach('files[]', Application.makePath('assets/Adonis.png'))

    assert.equal(res.body.title, post.title)
    assert.equal(res.body.content, newPost.content)
    assert.equal(res.body.topics[0].id, newPost.topics[0])
    assert.isNotNull(res.body.files)
  })

  test('should returns 422 when rate is not range between [1, 5]', async () => {
    const rate = { post_id: 1, rating: 0 }

    await request(BASE_URL)
      .post('/rate')
      .send(rate)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)

    await request(BASE_URL)
      .post('/rate')
      .send({ ...rate, rating: 6 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)
  })
  test('should returns 201 after rate post', async (assert) => {
    const rate1 = { post_id: 1, rating: 2 }
    const rate2 = { post_id: 1, rating: 5 }

    const res = await request(BASE_URL)
      .post('/rate')
      .send(rate1)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
    const post1 = res.body

    const res2 = await request(BASE_URL)
      .post('/rate')
      .send(rate2)
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
    const post2 = res2.body

    assert.deepEqual(post1.accounts[0].Rating, rate1.rating)
    assert.deepEqual(post2.accounts[1].Rating, rate2.rating)
    assert.deepEqual(post2.accounts.length, 2)
    assert.deepEqual(post1.id, post2.id)
    assert.notDeepEqual(post1.accounts[0].id, post2.accounts[1].id)
  })

  test('should returns 200 after show post', async (assert) => {
    const res = await request(BASE_URL)
      .get('/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.equal<string>(res.body.title, post.title)
    assert.equal<string>(res.body.content, post.content)
    assert.isFalse(res.body.is_comment_lock)
  })

  test('should returns 200 after show posts paginate', async (assert) => {
    const res = await request(BASE_URL)
      .get('/page/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.isObject(res.body.meta)
    assert.isArray<Post[]>(res.body.data)
  })

  test('should returns 200 after update post', async (assert) => {
    const p = await request(BASE_URL)
      .put('/update/1')
      .send({ ...post, title: 'Surfer INTECH', topics: [] })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)

    assert.isNotEmpty(p.body)
  })

  test('should returns 200 after lock/unlock comment on post', async (assert) => {
    const res = await request(BASE_URL)
      .get('/tooglelock/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.equal(
      res.body.message,
      'les commentaires sont désactivés',
      'Post failed to lock his comments.'
    )

    const res2 = await request(BASE_URL)
      .get('/tooglelock/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.equal(
      res2.body.message,
      'les commentaires sont activés',
      'Post failed to unlock his comments.'
    )
  })

  test('should returns 200 after delete/undo post', async (assert) => {
    const res = await request(BASE_URL)
      .delete('/delete/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.equal(res.body.message, 'Publication supprimée avec succès.', 'Post failed to delete.')

    await request(BASE_URL)
      .get('/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    await request(BASE_URL)
      .put('/update/1')
      .send({ ...post, title: 'Surfer INTECH', topics: [] })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    const res2 = await request(BASE_URL)
      .delete('/delete/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.equal(res2.body.message, 'Publication restaurée avec succès.', 'Post failed to undo.')
  })

  test('deep coverage tests', async () => {
    //coverage method tests
    await request(BASE_URL)
      .post('/rate')
      .send({ post_id: 1, rating: 3 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    await request(BASE_URL)
      .post('/rate')
      .send({ post_id: 10, rating: 3 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404)

    await request(BASE_URL)
      .put('/update/0')
      .send({ ...post, title: 'Surfer INTECH', topics: [] })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    request(BASE_URL)
      .put('/update/uio')
      .send({ ...post, title: 'Surfer INTECH', topics: [] })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    await request(BASE_URL)
      .get('/tooglelock/null')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .get('/tooglelock/10')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    await request(BASE_URL)
      .get('/0')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    await request(BASE_URL)
      .get('/ioe')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .get('/10')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    await request(BASE_URL)
      .get('/page/fioj')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .delete('/delete/keu')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)
    await request(BASE_URL)
      .delete('/delete/10')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)
  })
})
