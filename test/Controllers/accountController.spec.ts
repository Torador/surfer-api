import Application from '@ioc:Adonis/Core/Application'
//import Account from 'App/Models/Account'
import execa from 'execa'
import test from 'japa'
import request from 'supertest'
import { UNAUTHORIZED_PRIVILEGES } from '../../utils/const'
import { Jeton } from './../../utils/types'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}/api/v1/accounts`

let jetonUser3: Jeton
let jetonAdmin4: Jeton
export const user = {
  username: 'SurferUser',
  password: 'MySurferPassword',
  email: 'SurferUser@surfer.com',
}
export const admin = {
  username: 'SurferAdmin',
  password: 'AdminSurferPassword',
  role: 'admin',
  email: 'SurferAdmin@surfer.com',
}

export const accountTest = {
  user_id: 1,
  firstname: 'Jordan',
  lastname: 'Kagmeni',
  birthdate: '07/11/1998',
  email: 'kagmeni@et.intechinfo.fr',
  city: 'Cachan',
}
test.group('Test Accounts Controller', (group) => {
  group.before(async () => {
    const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}/api/v1/users`
    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send(admin)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    const userToken = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    const adminToken = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send(admin)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)

    jetonUser3 = JSON.parse(userToken.text)
    jetonAdmin4 = JSON.parse(adminToken.text)
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('should returns 401 when get accounts without auth', async () => {
    await request(BASE_URL)
      .get('', (err) => {
        console.log(err)
      })
      .expect(401)

    await request(BASE_URL)
      .post('', (err) => {
        console.log(err)
      })
      .send({
        firstname: 'Jordan',
        lastname: 'Kagmeni',
        birthdate: '07/11/1998',
        email: 'kagmeni@et.intechinfo.fr',
        city: 'Cachan',
      })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(401)
  })

  test('should returns 201 after create account on api', async () => {
    await request(BASE_URL)
      .post('', (err) => {
        if (err) throw new Error(err)
      })
      .send(accountTest)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
  })

  test('should returns 200 after update accounts with image upload', async () => {
    await request(BASE_URL)
      .put('/update/me')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .field('firstname', accountTest.firstname)
      .field('lastname', accountTest.lastname)
      .field('city', accountTest.city)
      .field('email', accountTest.email)
      .field('birthdate', accountTest.birthdate)
      .attach('picture', Application.makePath('assets/logo.png'))
      .expect(200)
  })

  test('should returns 200 after update accounts and replace existing image in drive', async () => {
    await request(BASE_URL)
      .put('/update/10')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .field('firstname', accountTest.firstname)
      .field('lastname', accountTest.lastname)
      .field('city', accountTest.city)
      .field('email', accountTest.email)
      .field('birthdate', accountTest.birthdate)
      .attach('picture', Application.makePath('assets/Adonis.png'))
      .expect(401)

    await request(BASE_URL)
      .put('/update/10')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .field('firstname', accountTest.firstname)
      .field('lastname', accountTest.lastname)
      .field('city', accountTest.city)
      .field('email', accountTest.email)
      .field('birthdate', accountTest.birthdate)
      .attach('picture', Application.makePath('assets/Adonis.png'))
      .expect(401)
    await request(BASE_URL)
      .put('/update/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .field('firstname', accountTest.firstname)
      .field('lastname', accountTest.lastname)
      .field('city', accountTest.city)
      .field('email', accountTest.email)
      .field('birthdate', accountTest.birthdate)
      .attach('picture', Application.makePath('assets/Adonis.png'))
      .expect(401)

    await request(BASE_URL)
      .put('/update/1')
      .set('Authorization', `bearer ${jetonAdmin4.token}`)
      .field('firstname', accountTest.firstname)
      .field('lastname', accountTest.lastname)
      .field('city', accountTest.city)
      .field('email', accountTest.email)
      .field('birthdate', accountTest.birthdate)
      .attach('picture', Application.makePath('assets/Adonis.png'))
      .expect(200)
  })

  test('should returns 200 after show paginate accounts', async (assert) => {
    const accounts = await request(BASE_URL)
      .get('/page/1/20')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)

    assert.isNotEmpty(accounts.body.meta)
    assert.isArray(accounts.body.data)
    assert.equal(accounts.body.data[0].user_id, 2)
  })

  test('should returns 200 when get accounts after auth', async (assert) => {
    const accounts = await request(BASE_URL)
      .get('')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.equal(accounts.body[2].firstname, accountTest.firstname)
    assert.equal(accounts.body[2].lastname, accountTest.lastname)
    assert.equal(accounts.body[2].email, user.email)
    assert.equal(accounts.body[2].city, accountTest.city)
  })

  test('should return 401 when account=>user with role "user" not try to delete account', async (assert) => {
    const error = await request(BASE_URL)
      .delete('/delete/2', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect(401)

    assert.equal(error.body.message, UNAUTHORIZED_PRIVILEGES)
  })

  test('should return 200 when accounts try to get his info without params', async () => {
    await request(BASE_URL)
      .get('/me', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect(200)
    await request(BASE_URL)
      .get('/me', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonAdmin4.token}`)
      .expect(200)
  })

  test('should return 200 when accounts try to get info with params', async () => {
    await request(BASE_URL)
      .get('/2', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect(200)
    await request(BASE_URL)
      .get('/2', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonAdmin4.token}`)
      .expect(200)
  })

  test('deep coverage tests', async () => {
    await request(BASE_URL)
      .post('', (err) => {
        if (err) throw new Error(err)
      })
      .send(accountTest)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)

    await request(BASE_URL)
      .get('/page/me')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .get('/20')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should returns 200 after accounts is lock/unlock', async (assert) => {
    const data = await request(BASE_URL)
      .get('/tooglelock/1', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect(200)
    assert.equal(data.body.message, 'Compte bloqué avec succès.')

    const data2 = await request(BASE_URL)
      .get('/tooglelock/1', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect(200)
    assert.equal(data2.body.message, 'Compte bloqué avec succès.')
  })

  test('should return 200 when account is delete', async (assert) => {
    const data = await request(BASE_URL)
      .delete('/delete/1', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect(200)
    assert.equal(data.body.message, 'Compte supprimé avec succès.')

    const data2 = await request(BASE_URL)
      .delete('/delete/1', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect(200)
    assert.equal(data2.body.message, 'Compte restauré avec succès.')

    const data3 = await request(BASE_URL)
      .delete('/delete/me', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect(200)
    assert.equal(data3.body.message, 'Compte supprimé avec succès.')
  })
})
