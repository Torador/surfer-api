import execa from 'execa'
import test from 'japa'
import request from 'supertest'
import { UNAUTHORIZED_PRIVILEGES, USER_NOT_FOUND } from '../../utils/const'
import { Jeton } from './../../utils/types'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}/api/v1/users`

test.group('Test Users Controller', (group) => {
  let jetonUser1: Jeton
  let jetonAdmin2: Jeton
  const user = {
    username: 'SurferUser',
    password: 'MySurferPassword',
    email: 'SurferUser@surfer.com',
  }
  const admin = {
    username: 'SurferAdmin',
    password: 'AdminSurferPassword',
    role: 'admin',
    email: 'SurferAdmin@surfer.com',
  }
  const admin2 = {
    username: 'admin',
    password: admin.password,
    role: admin.role,
    email: 'admin@surfer.com',
  }

  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('should returns 302, 200 or 500 when we get socials auth', async () => {
    const HOST = `http://${process.env.HOST}:${process.env.PORT}`
    await request(HOST).get('/google/redirect').expect(302)
    await request(HOST).get('/github/redirect').expect(200)
    await request(HOST).get('/discord/redirect').expect(302)
    await request(HOST).get('/twitter/redirect').expect(500)
    await request(HOST).get('/facebook/redirect').expect(302)
    await request(HOST).get('/linkedin/redirect').expect(302)
  })

  test('should returns 401 when get users without auth', async () => {
    await request(BASE_URL)
      .get('', (err) => {
        console.log(err)
      })
      .expect(401)
  })

  test('should returns 201 after register on api', async () => {
    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
  })

  test('should returns 422 after bad body in request: wrong validating payload', async (assert) => {
    const error = await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send({ username: 'toto' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)

    assert.equal(error.body.errors[0].field, 'password')
    assert.equal(error.body.errors[0].message, 'Veuillez entrer votre mot de passe')
  })

  test('should returns 200 after signIn with correct credentials', async (assert) => {
    const token = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)
    jetonUser1 = JSON.parse(token.text)
    assert.equal(token.body.type, 'bearer')
    assert.isNotNull(token.body.token)
  })

  test('should returns 200 when get users after auth', async (assert) => {
    const users = await request(BASE_URL)
      .get('')
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.equal(users.body[0].username, user.username)
    assert.equal(users.body[0].role, 'user')
  })

  test('should return 400 after sign in with bad credentials', async () => {
    await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...user, password: 'qwertyuiop' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(400)
  })

  test('should returns 404 when user not found during login', async (assert) => {
    const error = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send({ username: 'totoaimetiti', password: 'qwertyuiop', email: 'totoaimetiti@gmail.com' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404)
    assert.equal(error.body.message, USER_NOT_FOUND)
  })

  test('should returns 401 when user with role "user" not try to delete user', async (assert) => {
    const error = await request(BASE_URL)
      .delete('/delete/2', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .expect(401)

    assert.equal(error.body.message, UNAUTHORIZED_PRIVILEGES)
  })

  test('should returns 401 when user with role "user" try to get his info', async () => {
    await request(BASE_URL)
      .get('/show/me', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .expect(401)
  })

  test('should returns 200 when user with role "admin" try to get info', async (assert) => {
    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send(admin2)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    const token = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send({ username: admin2.username, password: admin2.password, email: admin2.email })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)

    jetonAdmin2 = JSON.parse(token.text)

    const currentAdmin = await request(BASE_URL)
      .get('/me', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonAdmin2.token}`)
      .expect(200)

    const currentUser = await request(BASE_URL)
      .get('/1', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonAdmin2.token}`)
      .expect(200)

    assert.equal(currentAdmin.body.username, admin2.username)
    assert.equal(currentUser.body.username, user.username)
    assert.notStrictEqual(currentAdmin.body.username, currentUser.body.username)
  })

  test('should returns 200 when user updated info', async () => {
    await request(BASE_URL)
      .put('/update/me')
      .send({ username: admin2.username, password: 'newPassword2021', email: admin2.email })
      .set('Authorization', `bearer ${jetonAdmin2.token}`)
      .expect(200)

    await request(BASE_URL)
      .put('/update/1')
      .send({ username: user.username, password: 'newPassword2021', email: admin2.email })
      .set('Authorization', `bearer ${jetonAdmin2.token}`)
      .expect(200)

    await request(BASE_URL)
      .put('/update/me')
      .send({ username: user.username, password: 'newPassword2021', email: user.email })
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .expect(200)
  })

  test('should returns 401 when user with role "user" update role user', async () => {
    await request(BASE_URL)
      .get('/admin/1')
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .expect(401)
  })

  test('should returns 200 when user with role "admin" update role user', async () => {
    await request(BASE_URL)
      .get('/1')
      .set('Authorization', `bearer ${jetonAdmin2.token}`)
      .expect(200)
  })

  test('should returns 401 when user with role "user" try to update info to another user', async () => {
    await request(BASE_URL)
      .put('/update/2')
      .send({ username: user.username, password: 'newPassword2021', email: user.email })
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .expect(401)
  })

  test('should returns 200 when user logout after login', async (assert) => {
    const data = await request(BASE_URL)
      .get('/logout', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .expect(200)
    assert.equal(data.body.message, `À la prochaine ${user.username}.`)
  })

  test('should returns 401 when user try to acces to protected ressources after logout', async () => {
    await request(BASE_URL)
      .get('/me', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .expect(401)
  })

  test('should returns 200 when user with role "admin" delete user', async () => {
    await request(BASE_URL)
      .delete('/delete/1', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonAdmin2.token}`)
      .expect(200)
    await request(BASE_URL)
      .delete('/delete/me', (err) => {
        if (err) throw new Error(err)
      })
      .set('Authorization', `bearer ${jetonAdmin2.token}`)
      .expect(200)
  })
})
