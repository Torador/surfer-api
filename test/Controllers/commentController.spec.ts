import test from 'japa'
import request from 'supertest'
//import Post from 'App/Models/Post'
import Comment from 'App/Models/Comment'
import execa from 'execa'
import { user } from './accountController.spec'
import { Jeton } from '../../utils/types'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}/api/v1/comments`

test.group('Test Controller Comments', async (group) => {
  let jetonUser3: Jeton
  let jetonUser1: Jeton

  const comment = {
    content: 'Tout le monde mange la  banane',
  }
  const topic = {
    id: 1,
    label: 'label',
    description: 'description',
  }
  const post = {
    title: 'titre',
    content: 'contenu',
    topics: [1],
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}/api/v1/users`

    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...user, username: 'Toto', email: 'toto@gmail.com' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    const userToken = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)

    const userToken2 = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...user, username: 'Toto', email: 'toto@gmail.com' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)

    jetonUser3 = JSON.parse(userToken.text)
    jetonUser1 = JSON.parse(userToken2.text)

    await request(BASE_URL.replace('users', 'topics'))
      .post('', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...topic })
      .set('Accept', 'application/json')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(201)

    await request(BASE_URL.replace('users', 'posts'))
      .post('', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...post })
      .set('Accept', 'application/json')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(201)
    await request(BASE_URL.replace('users', 'comments'))
      .post('', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...comment, postId: 1 })
      .set('Accept', 'application/json')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(201)
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })

  test('should returns 422 when save comment without content', async () => {
    const newComment = {}

    await request(BASE_URL)
      .post('')
      .send(newComment)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)
  })
  test('should returns 404 when save comment without post', async () => {
    const newComment = { ...comment, postId: 7 }

    await request(BASE_URL)
      .post('')
      .send(newComment)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404)
  })

  test('should returns 422 when save comment without account id', async () => {
    await request(BASE_URL)
      .post('')
      .send(comment)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)
  })

  test('should returns 401 when save comment without auth user', async () => {
    await request(BASE_URL).post('').send(comment).expect(401)
  })

  test('should returns 422 when rate is not range between [1, 5]', async () => {
    const rate = { comment_id: 1, rating: 0 }

    await request(BASE_URL)
      .post('/rate')
      .send(rate)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)

    await request(BASE_URL)
      .post('/rate')
      .send({ ...rate, rating: 6 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)
  })

  test('should returns 201 after rate comment', async (assert) => {
    const rate1 = { comment_id: 1, rating: 2 }
    const rate2 = { comment_id: 1, rating: 5 }

    const res = await request(BASE_URL)
      .post('/rate')
      .send(rate1)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
    const comment1 = res.body

    const res2 = await request(BASE_URL)
      .post('/rate')
      .send(rate2)
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
    const comment2 = res2.body

    assert.deepEqual(comment1.accounts[0].Rating, rate1.rating)
    assert.deepEqual(comment2.accounts[1].Rating, rate2.rating)
    assert.deepEqual(comment2.accounts.length, 2)
    assert.deepEqual(comment1.id, comment2.id)
    assert.notDeepEqual(comment1.accounts[0].id, comment2.accounts[1].id)
  })

  test('should returns 200 after show comment', async (assert) => {
    const res = await request(BASE_URL)
      .get('/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal<number>(res.body[0].post_id, 1)
    assert.equal<string>(res.body[0].content, comment.content)
  })

  test('should returns 200 after show comments paginate', async (assert) => {
    const res = await request(BASE_URL)
      .get('/page/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.isObject(res.body.meta)
    assert.isArray<Comment[]>(res.body.data)
  })
  test('should returns 400 after show comments paginate with invalide page number', async () => {
    await request(BASE_URL)
      .get('/page/-5')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)
    await request(BASE_URL)
      .get('/page/r')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)
  })

  test('should returns 200 after update comment', async (assert) => {
    const c = await request(BASE_URL)
      .put('/update/1')
      .send({ content: 'ceci est un nouveau commentaire', postId: 1 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)

    assert.isNotEmpty(c.body)
  })

  test('should returns 400 after update comment with invalid user', async (assert) => {
    const c = await request(BASE_URL)
      .put('/update/kk')
      .send({ content: 'ceci est un nouveau commentaire', postId: 1 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    assert.isNotEmpty(c.body)
  })

  test('should returns 200 after delete comment', async (assert) => {
    const res = await request(BASE_URL)
      .delete('/delete/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.equal(res.body.message, 'Commentaire supprimé avec succès.', 'Comment failed to delete.')
  })

  test('deep coverage tests', async () => {
    //coverage method tests
    await request(BASE_URL)
      .post('/rate')
      .send({ comment_id: 1, rating: 3 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    await request(BASE_URL)
      .post('/rate')
      .send({ comment_id: 10, rating: 3 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404)

    await request(BASE_URL)
      .put('/update/0')
      .send({ ...comment, content: 'Surfer INTECH', postId: 1 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    request(BASE_URL)
      .put('/update/uio')
      .send({ ...comment, content: 'Surfer INTECH', postId: 1 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .get('/0')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .get('/abc')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .get('/page/fioj')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .delete('/delete/keu')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .delete('/delete/10')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)
  })
})
