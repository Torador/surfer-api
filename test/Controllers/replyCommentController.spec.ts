import test from 'japa'
import request from 'supertest'
import ReplyComment from 'App/Models/ReplyComment'
import execa from 'execa'
import { user } from './accountController.spec'
import { Jeton } from '../../utils/types'

const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}/api/v1/reply`

test.group('Test Controller ReplyComments', async (group) => {
  let jetonUser3: Jeton
  let jetonUser1: Jeton
  const reply = {
    content: 'ceci est une reponse',
  }

  const comment = {
    content: 'Tout le monde mange la  banane',
  }
  const topic = {
    id: 1,
    label: 'label',
    description: 'description',
  }
  const post = {
    title: 'titre',
    content: 'contenu',
    topics: [1],
  }
  group.before(async () => {
    await execa.node('ace', ['migration:run'], { stdio: 'inherit' })

    const BASE_URL = `http://${process.env.HOST}:${process.env.PORT}/api/v1/users`

    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    await request(BASE_URL)
      .post('/register', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...user, username: 'Toto', email: 'toto@gmail.com' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)

    const userToken = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send(user)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)

    const userToken2 = await request(BASE_URL)
      .post('/login', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...user, username: 'Toto', email: 'toto@gmail.com' })
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200)

    jetonUser3 = JSON.parse(userToken.text)
    jetonUser1 = JSON.parse(userToken2.text)

    await request(BASE_URL.replace('users', 'topics'))
      .post('', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...topic })
      .set('Accept', 'application/json')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(201)

    await request(BASE_URL.replace('users', 'posts'))
      .post('', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...post })
      .set('Accept', 'application/json')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(201)
    await request(BASE_URL.replace('users', 'comments'))
      .post('', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...comment, postId: 1 })
      .set('Accept', 'application/json')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(201)
    await request(BASE_URL.replace('users', 'reply'))
      .post('', (err) => {
        if (err) throw new Error(err)
      })
      .send({ ...reply, commentId: 1, accountId: 1 })
      .set('Accept', 'application/json')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(201)
  })

  group.after(async () => {
    await execa.node('ace', ['migration:rollback', '--batch=0'], { stdio: 'inherit' })
  })
  test('should returns 422 when save reply without content', async () => {
    const newReplyComment = { content: '', commentId: 1, accountId: 1 }

    await request(BASE_URL)
      .post('')
      .send(newReplyComment)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)
  })
  test('should returns 404 when save reply with invalid commentId', async () => {
    const newReplyComment = { content: 'ceci est une reponse', commentId: 10, accountId: 1 }

    await request(BASE_URL)
      .post('')
      .send(newReplyComment)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404)
  })
  test('should returns 422 when save reply without account id', async () => {
    await request(BASE_URL)
      .post('')
      .send(reply)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)
  })
  test('should returns 401 when save reply without auth user', async () => {
    await request(BASE_URL).post('').send(comment).expect(401)
  })
  test('should returns 422 when rate is not range between [1, 5]', async () => {
    const rate = { reply_id: 1, rating: 0 }

    await request(BASE_URL)
      .post('/rate')
      .send(rate)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)

    await request(BASE_URL)
      .post('/rate')
      .send({ ...rate, rating: 6 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(422)
  })
  test('should returns 201 after rate reply', async (assert) => {
    const rate1 = { reply_id: 1, rating: 2 }
    const rate2 = { reply_id: 1, rating: 5 }

    const res = await request(BASE_URL)
      .post('/rate')
      .send(rate1)
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
    const reply1 = res.body

    const res2 = await request(BASE_URL)
      .post('/rate')
      .send(rate2)
      .set('Authorization', `bearer ${jetonUser1.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(201)
    const reply2 = res2.body

    assert.deepEqual(reply1.replies[0].Rating, rate1.rating)
    assert.deepEqual(reply2.replies[1].Rating, rate2.rating)
    assert.deepEqual(reply2.replies.length, 2)
    assert.deepEqual(reply1.id, reply2.id)
    assert.notDeepEqual(reply1.replies[0].id, reply2.replies[1].id)
  })
  test('should returns 200 after show reply', async (assert) => {
    const res = await request(BASE_URL)
      .get('/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)

    assert.equal<number>(res.body[0].comment_id, 1)
    assert.equal<string>(res.body[0].content, reply.content)
  })
  test('should returns 200 after show reply paginate', async (assert) => {
    const res = await request(BASE_URL)
      .get('/page/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.isObject(res.body.meta)
    assert.isArray<ReplyComment[]>(res.body.data)
  })
  test('should returns 400 after show replies paginate with invalide page number', async () => {
    await request(BASE_URL)
      .get('/page/-5')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)
    await request(BASE_URL)
      .get('/page/r')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)
  })
  test('should returns 200 after update reply', async (assert) => {
    const r = await request(BASE_URL)
      .put('/update/1')
      .send({ content: 'ceci est une nouvelle reponse', commentId: 1 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)

    assert.isNotEmpty(r.body)
  })
  test('should returns 400 after update reply with invalid user', async (assert) => {
    const r = await request(BASE_URL)
      .put('/update/ABC')
      .send({ content: 'ceci est une nouvelle reponse', commentId: 1 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)
    assert.isNotEmpty(r.body)
  })
  test('should returns 200 after delete reply', async (assert) => {
    const res = await request(BASE_URL)
      .delete('/delete/1')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(200)
    assert.equal(res.body.message, 'Reponse supprimée.', 'Reply failed to delete.')
  })
  test('should returns 400 after delete reply with invalide reply_id', async () => {
    await request(BASE_URL)
      .delete('/delete/A')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)
  })

  test('deep coverage tests', async () => {
    //coverage method tests

    await request(BASE_URL)
      .post('/rate')
      .send({ reply_id: 10, comment_id: 1, rating: 3 })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(404)

    request(BASE_URL)
      .put('/update/uio')
      .send({ ...reply, content: 'Surfer INTECH' })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    request(BASE_URL)
      .put('/update/10')
      .send({ ...reply, content: 'Surfer INTECH' })
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    await request(BASE_URL)
      .get('/0')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .get('/page/fioj')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .delete('/delete/keu')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)

    await request(BASE_URL)
      .delete('/delete/10')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(404)

    await request(BASE_URL)
      .get('/page/-5')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)
    await request(BASE_URL)
      .get('/page/r')
      .set('Authorization', `bearer ${jetonUser3.token}`)
      .expect('Content-Type', /json/)
      .expect(400)
  })
})
