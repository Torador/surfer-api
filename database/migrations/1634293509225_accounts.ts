import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class Accounts extends BaseSchema {
  protected tableName = 'accounts'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table
        .integer('user_id')
        .unsigned()
        .references('id')
        .inTable('users')
        .notNullable()
        .onDelete('CASCADE')
      table.string('firstname', 100)
      table.string('lastname', 100)
      table.date('birthdate')
      table.string('slug', 255).notNullable().unique()
      table.string('address', 255)
      table.string('code_postal', 20)
      table.string('city', 100)
      table.string('phone', 200)
      table.string('job', 255)
      table.string('email', 255).unique().notNullable()
      table.string('photo', 255)
      table.text('bio')
      table.enum('visibility', ['public', 'private']).notNullable().defaultTo('public')
      table.string('code_backup', 255)
      table.boolean('status').notNullable().defaultTo(false)
      table.boolean('is_lock').notNullable().defaultTo(false)
      table.boolean('is_delete').notNullable().defaultTo(false)

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
