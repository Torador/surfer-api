import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class IsFollows extends BaseSchema {
  protected tableName = 'is_follows'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.integer('contact_id').unsigned().references('id').inTable('contacts').notNullable()
      table.integer('follower_id').unsigned().references('id').inTable('followers').notNullable()
      table.unique(['contact_id', 'follower_id'])

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
