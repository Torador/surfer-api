import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class FilePosts extends BaseSchema {
  protected tableName = 'file_posts'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table
        .integer('post_id')
        .unsigned()
        .references('id')
        .inTable('posts')
        .notNullable()
        .onDelete('CASCADE')
      table.integer('size').unsigned().notNullable()
      table.string('mime_type', 20).notNullable()
      table.string('filename', 100).notNullable()
      table.string('url_img', 255).notNullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
