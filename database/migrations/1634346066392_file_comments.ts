import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class FileComments extends BaseSchema {
  protected tableName = 'file_comments'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.integer('comment_id').unsigned().references('id').inTable('comments').notNullable()
      table.integer('size').unsigned().notNullable()
      table.string('mime_type', 20).notNullable()
      table.string('filename', 100).notNullable()
      table.string('url_img', 255).notNullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
