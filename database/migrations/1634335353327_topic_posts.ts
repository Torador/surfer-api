import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class TopicPosts extends BaseSchema {
  protected tableName = 'topic_posts'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table.integer('topic_id').unsigned().references('id').inTable('topics').notNullable()
      table.integer('post_id').unsigned().references('id').inTable('posts').notNullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
