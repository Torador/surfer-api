import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class MediaMessages extends BaseSchema {
  protected tableName = 'media_messages'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id').primary()
      table
        .integer('message_id')
        .unsigned()
        .references('id')
        .inTable('messages')
        .notNullable()
        .onDelete('CASCADE')
      table.string('media_name', 100).notNullable()
      table.string('url_media', 255).notNullable()
      table.string('mime_type', 20).notNullable()
      table.integer('size').unsigned().notNullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
