import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class RatingReplies extends BaseSchema {
  protected tableName = 'rating_replies'

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table
        .integer('account_id')
        .unsigned()
        .references('id')
        .inTable('accounts')
        .notNullable()
        .onDelete('CASCADE')
      table.integer('reply_id').unsigned().references('id').inTable('reply_comments').notNullable()
      table.integer('rating', 5).unsigned().notNullable()

      /**
       * Uses timestamptz for PostgreSQL and DATETIME2 for MSSQL
       */
      table.timestamp('created_at', { useTz: true })
      table.timestamp('updated_at', { useTz: true })
    })
  }

  public async down() {
    this.schema.dropTable(this.tableName)
  }
}
