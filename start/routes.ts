import Application from '@ioc:Adonis/Core/Application'
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'
import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  Route.get('/', async () => {
    return {
      title: `WELCOME TO ${Application.appName.toUpperCase()} VERSION ${Application.version?.toString()}`,
      technologie: `AdonisJS ${Application.adonisVersion?.toString()}`,
      path: `${Application.appRoot}`,
      node_env: `${Application.nodeEnvironment}`,
      ready: Application.isReady,
    }
  })

  Route.get('/health', async ({ response }) => {
    const report = await HealthCheck.getReport()

    return report.healthy ? response.ok(report) : response.badRequest(report)
  })
}).middleware('logrequest')

Route.group(() => {
  Route.group(() => {
    Route.group(() => {
      Route.get('', 'UsersController.showAll')
      Route.get('/logout', 'UsersController.logout')
      Route.group(() => {
        Route.get('/:page', 'UsersController.index')
        Route.get('/:page/:limit', 'UsersController.index')
      }).prefix('/page')
      Route.get('/:id', 'UsersController.show')
      Route.get('/:role/:id', 'UsersController.updateRole')
      Route.put('/update/:id', 'UsersController.update')
      Route.delete('/delete/:id', 'UsersController.delete')
    }).middleware('auth')
    Route.post('/register', 'UsersController.register')
    Route.post('/login', 'UsersController.login')
  }).prefix('/users')

  Route.group(() => {
    Route.post('', 'AccountsController.create')
    Route.get('', 'AccountsController.showAll')
    Route.group(() => {
      Route.get('/:page/', 'AccountsController.index')
      Route.get('/:page/:limit', 'AccountsController.index')
    }).prefix('/page')
    Route.get('/:id', 'AccountsController.show')
    Route.put('/update/:id', 'AccountsController.update')
    Route.get('/slug/:slug', 'AccountsController.showBySlug')
    Route.get('/tooglelock/:id', 'AccountsController.toggleLock')
    Route.get('/:visibility/:id', 'AccountsController.updateVisibility')
    Route.delete('/delete/:id', 'AccountsController.destroyOrUndo')
  })
    .prefix('/accounts')
    .middleware('auth')

  Route.get('/token/check', ({ response, auth }) => {
    return response.ok(auth.isAuthenticated)
  }).middleware('auth')

  //routes de posts
  Route.group(() => {
    Route.post('', 'PostsController.create')
    Route.post('/rate', 'PostsController.rate')
    Route.get('/slug/:slug', 'PostsController.showBySlug')
    //pagination
    Route.group(() => {
      Route.get('/:page', 'PostsController.index')
      Route.get('/:page/:limit', 'PostsController.index')
    }).prefix('/page')
    //authorizeComments
    Route.get('/tooglelock/:id', 'PostsController.authorizeComments')
    Route.get('/:id', 'PostsController.show')
    Route.delete('/delete/:id', 'PostsController.delete')
    Route.put('/update/:id', 'PostsController.update')
  })
    .prefix('/posts')
    .middleware('auth')

  //route des commentaires
  Route.group(() => {
    Route.group(() => {
      Route.get('/:page', 'CommentsController.index')
      Route.get('/:page/:limit', 'CommentsController.index')
    }).prefix('/page')
    Route.get('/:postId', 'CommentsController.getByPost')
    Route.post('', 'CommentsController.create')
    Route.post('/rate', 'CommentsController.rate')
    Route.put('/update/:id', 'CommentsController.update')
    Route.delete('/delete/:id', 'CommentsController.delete')
  })
    .prefix('/comments')
    .middleware('auth')

  //Routes Follow
  Route.group(() => {
    Route.get('/suggestions', 'FollowersController.suggestionContact')
    Route.get('/followers', 'FollowersController.showFollowers')
    Route.get('/following', 'FollowersController.showFollowing')
    Route.get('/create/:id', 'FollowersController.store')
    Route.delete('/delete/:followingId', 'FollowersController.destroy')
    Route.get('/check/:followingId', 'FollowersController.isAlreadyFollow')
  })
    .middleware('auth')
    .prefix('/follow')

  //routes de topics
  Route.group(() => {
    Route.get('', 'TopicsController.index')
    Route.post('', 'TopicsController.create')
  })
    .prefix('/topics')
    .middleware('auth')

  //routes des reply comments
  Route.group(() => {
    Route.group(() => {
      Route.get('/:page', 'ReplyCommentsController.index')
      Route.get('/:page/:limit', 'ReplyCommentsController.index')
    }).prefix('/page')
    Route.get('/:commentId', 'ReplyCommentsController.getByComment')
    Route.post('', 'ReplyCommentsController.create')
    Route.post('/rate', 'ReplyCommentsController.rate')
    Route.put('/update/:id', 'ReplyCommentsController.update')
    Route.delete('/delete/:id', 'ReplyCommentsController.delete')
  })
    .prefix('/reply')
    .middleware('auth')

  Route.group(() => {
    Route.post('', 'ConversationsController.create')
    Route.group(() => {
      Route.get('/:page', 'ConversationsController.index')
      Route.get('/:page/:limit', 'ConversationsController.index')
    }).prefix('/page')
    Route.get('', 'ConversationsController.showAll')
    Route.delete('/delete/:id', 'ConversationsController.delete')
  })
    .prefix('/conversations')
    .middleware('auth')

  Route.group(() => {
    Route.get('/:conversationId', 'MessagesController.showAll')
    Route.get('/:conversationId/:page', 'MessagesController.index')
    Route.get('/:conversationId/:page/:limit', 'MessagesController.index')
    Route.post('', 'MessagesController.create')
    Route.delete('/delete/:id', 'MessagesController.delete')
  })
    .prefix('/messages')
    .middleware('auth')
})
  .prefix('/api/v1')
  .middleware('logrequest')

/**
 * DELEGATES AUTHENTIFICATIONS
 * @description All routes for auth with social platform
 * @author Torador
 */
Route.group(() => {
  Route.group(() => {
    Route.get('/redirect', async ({ ally, request }) => {
      if (!request.hasValidSignature()) {
        return 'Signature is missing or URL was tampered.'
      }
      return ally.use('github').redirect()
    })
    Route.get('/callback', 'UsersController.authWithGithub')
  }).prefix('/github')

  Route.group(() => {
    Route.get('/redirect', async ({ ally }) => {
      return ally.use('google').redirect()
    })
    Route.get('/callback', 'UsersController.authWithGoogle')
  }).prefix('/google')

  Route.group(() => {
    Route.get('/redirect', async ({ ally }) => {
      return ally.use('twitter').redirect()
    })
    Route.get('/callback', 'UsersController.authWithTwitter')
  }).prefix('/twitter')

  Route.group(() => {
    Route.get('/redirect', async ({ ally }) => {
      return ally.use('discord').redirect()
    })
    Route.get('/callback', 'UsersController.authWithDiscord')
  }).prefix('/discord')

  Route.group(() => {
    Route.get('/redirect', async ({ ally }) => {
      return ally.use('linkedin').redirect()
    })
    Route.get('/callback', 'UsersController.authWithLinkedin')
  }).prefix('/linkedin')

  Route.group(() => {
    Route.get('/redirect', async ({ ally }) => {
      return ally.use('facebook').redirect()
    })
    Route.get('/callback', 'UsersController.authWithFacebook')
  }).prefix('/facebook')
}).middleware('logrequest')
