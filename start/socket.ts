import Redis from '@ioc:Adonis/Addons/Redis'
import Account from 'App/Models/Account'
import Message from 'App/Models/Message'
import Ws from '../app/Services/Ws'
Ws.boot()

type JoinRoom = {
  room: string
  account: Account
}

type PayloadMessage = {
  message: Message
  room: string
}

/**
 * Listen for incoming socket connections
 */
Ws.io.on('connection', async (socket) => {
  socket.on('user:new', async (slug: string, callback) => {
    try {
      await Redis.set(slug, socket.id)
    } catch (error) {
      console.log(error)
      callback(error)
    }
  })

  socket.on('conversation:new', async (room: string, cb) => {
    socket.join(room)
    cb(`Joined room: ${room}`)
  })

  socket.on('conversation:join', async ({ room }: JoinRoom, callback) => {
    try {
      console.log('Joined ' + room)

      //await Redis.set(socket.id, JSON.stringify({ room, ...account }))
      socket.join(room)
      callback()
    } catch (error) {
      return callback(error)
    }
  })

  socket.on('message:new', async ({ message, room }: PayloadMessage) => {
    socket.broadcast.to(room).emit('message:comming', message)
  })

  socket.on('message:delete', async ({ message, room }: PayloadMessage) => {
    socket.to(room).emit('message:deleted', message)
  })

  socket.emit('msg', await Account.all())
})
