import Env from '@ioc:Adonis/Core/Env'
import { OpaqueTokenContract } from '@ioc:Adonis/Addons/Auth'
import Logger from '@ioc:Adonis/Core/Logger'
import { ResponseContract } from '@ioc:Adonis/Core/Response'
import User from 'App/Models/User'
export default class Utilities {
  // eslint-disable-next-line @typescript-eslint/explicit-member-accessibility
  static ControllerException(response: ResponseContract, error: any) {
    Logger.error(error)
    return response.internalServerError(error.message)
  }
}

export function responseAfterAuth(
  response: ResponseContract,
  token: OpaqueTokenContract<User>
): ResponseContract {
  response.redirect(Env.get('FRONT_HOST') + '/' + token.token)
  return response
}
