export const ACCOUNT_NOT_FOUND = "Ce compte n'existe pas."
export const POST_NOT_FOUND = "Ce poste n'existe pas."
export const POST_DELETED = 'Ce poste a été supprimé .'
export const USER_NOT_FOUND = "Cet utilisateur n'existe pas."
export const CONTACT_NOT_FOUND = "Ce contact n'existe pas."
export const FOLLOWING_NOT_FOUND = "Le follower n'existe pas."
export const COMMENT_NOT_FOUND = "Ce commentaire n'existe pas."
export const REPLY_COMMENT_NOT_FOUND = "Cette reponse n'existe pas."
export const ID_NOT_DEFINE = "L'identifiant du compte n'est pas renseigné."
export const PAGE_NUMBER_INVALID = 'Le numéro de la page est invalid.'
export const ACCOUNT_DELETED = 'Ce compte a été suprimmé du système.'
export const ACCOUNT_LOCKED = 'Ce compte a été bloqué.'
export const INVALID_CREDENTIALS = "Nom d'utilisateur ou mot de passe incorrect."
export const UNAUTHORIZED_PRIVILEGES =
  "Vous n'avez pas les droits requis pour éffectuer cette action."
export const IMAGES_EXTENSION = ['jpg', 'png', 'jpeg', 'gif', 'svg']
